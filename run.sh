#!/bin/bash

# If Essence sources are in a sibling folder called src we can try to build the api header file prior
# to running Doxygen / Doxybook and Hugo
ESSENCE_SOURCES_DIR="../src"
DOXYGEN_SOURCES_DIR="doxygen/src"

echo "Generating Essence documentation"

if [[ -d "$ESSENCE_SOURCES_DIR" ]]; then
  if [[ ! -d "$DOXYGEN_SOURCES_DIR" ]]; then
    mkdir "$DOXYGEN_SOURCES_DIR"
  fi

  echo "Generating Essence header file"
  OLD_PATH=`pwd`
  cd "$ESSENCE_SOURCES_DIR"
  
  gcc -o bin/build_core util/build_core.c
  bin/build_core headers c ../doc/doxygen/src/api.h

  if [[ "$?" -ne "0" ]]; then
    echo "[ERROR]: Failed generating the api header file"
    exit 1
  fi
fi

cd $OLD_PATH/doxygen
./generate_doc.sh

if [[ "$?" -ne "0" ]]; then
  echo "[ERROR]: Failed generating the doxygen xml documentation"
  exit 1
fi

cd $OLD_PATH/doxybook

./generate_doc.sh

if [[ "$?" -ne "0" ]]; then
  echo "[ERROR]: Failed generating the doxybook documentation"
  exit 1
fi

cd $OLD_PATH/website
./deploy_doc.sh

if [[ "$?" -ne "0" ]]; then
  echo "[ERROR]: Failed deploying the documentation"
  exit 1
fi

cd $OLD_PATH
echo "[SUCCESS]: The documentation has been deployed"

