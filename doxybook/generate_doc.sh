#!/bin/bash

INPUT_DIR="../doxygen/xml/"
OUTPUT_DIR="output/"
TEMPLATE_DIR="templates/"

if [[ ! -d "$OUTPUT_DIR" ]]; then
  mkdir "$OUTPUT_DIR"
fi

doxybook2 --input "$INPUT_DIR" --output "$OUTPUT_DIR" --templates "$TEMPLATE_DIR"