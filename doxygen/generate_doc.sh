#!/bin/bash

if [[ ! -f "src/api.h" ]]; then
  echo "Missing api header file. Expected: src/api.h"
  exit 1
fi

doxygen essence_api.doxyfile