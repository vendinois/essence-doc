#!/bin/bash

AUTOGENDOCS_DIR="content/autogendocs/"
GENERATED_DOC_DIR="../doxybook/output"

rm -r "$AUTOGENDOCS_DIR/Classes"
cp -r "$GENERATED_DOC_DIR/Classes" "$AUTOGENDOCS_DIR"

git add "$AUTOGENDOCS_DIR"
git commit -am "Updated auto generated documentation"
git push
