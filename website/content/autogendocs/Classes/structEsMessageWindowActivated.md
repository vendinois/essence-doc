---
title: EsMessageWindowActivated

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint8_t | **[leftModifiers](Classes/structEsMessageWindowActivated.md#variable-leftmodifiers)**  |
| uint8_t | **[rightModifiers](Classes/structEsMessageWindowActivated.md#variable-rightmodifiers)**  |

## Public Attributes Documentation

### variable leftModifiers

```cpp
uint8_t leftModifiers;
```


### variable rightModifiers

```cpp
uint8_t rightModifiers;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100