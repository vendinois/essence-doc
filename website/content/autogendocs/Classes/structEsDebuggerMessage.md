---
title: EsDebuggerMessage

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[process](Classes/structEsDebuggerMessage.md#variable-process)**  |
| [EsCrashReason](Classes/structEsCrashReason.md) | **[reason](Classes/structEsDebuggerMessage.md#variable-reason)**  |

## Public Attributes Documentation

### variable process

```cpp
EsHandle process;
```


### variable reason

```cpp
EsCrashReason reason;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100