---
title: _EsApplicationStartupInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int64_t | **[id](Classes/struct__EsApplicationStartupInformation.md#variable-id)**  |
| const char * | **[filePath](Classes/struct__EsApplicationStartupInformation.md#variable-filepath)**  |
| ptrdiff_t | **[filePathBytes](Classes/struct__EsApplicationStartupInformation.md#variable-filepathbytes)**  |
| [EsWindow](Files/api_8h.md#define-eswindow) * | **[targetWindow](Classes/struct__EsApplicationStartupInformation.md#variable-targetwindow)**  |
| uint32_t | **[flags](Classes/struct__EsApplicationStartupInformation.md#variable-flags)**  |
| int32_t | **[data](Classes/struct__EsApplicationStartupInformation.md#variable-data)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[readHandle](Classes/struct__EsApplicationStartupInformation.md#variable-readhandle)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[documentID](Classes/struct__EsApplicationStartupInformation.md#variable-documentid)**  |
| const char * | **[containingFolder](Classes/struct__EsApplicationStartupInformation.md#variable-containingfolder)**  |
| ptrdiff_t | **[containingFolderBytes](Classes/struct__EsApplicationStartupInformation.md#variable-containingfolderbytes)**  |

## Public Attributes Documentation

### variable id

```cpp
int64_t id;
```


### variable filePath

```cpp
const char * filePath;
```


### variable filePathBytes

```cpp
ptrdiff_t filePathBytes;
```


### variable targetWindow

```cpp
EsWindow * targetWindow;
```


### variable flags

```cpp
uint32_t flags;
```


### variable data

```cpp
int32_t data;
```


### variable readHandle

```cpp
EsHandle readHandle;
```


### variable documentID

```cpp
EsObjectID documentID;
```


### variable containingFolder

```cpp
const char * containingFolder;
```


### variable containingFolderBytes

```cpp
ptrdiff_t containingFolderBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100