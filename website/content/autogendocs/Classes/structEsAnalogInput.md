---
title: EsAnalogInput

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint8_t | **[x](Classes/structEsAnalogInput.md#variable-x)**  |
| uint8_t | **[y](Classes/structEsAnalogInput.md#variable-y)**  |
| uint8_t | **[z](Classes/structEsAnalogInput.md#variable-z)**  |

## Public Attributes Documentation

### variable x

```cpp
uint8_t x;
```


### variable y

```cpp
uint8_t y;
```


### variable z

```cpp
uint8_t z;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100