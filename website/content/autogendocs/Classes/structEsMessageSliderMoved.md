---
title: EsMessageSliderMoved

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| double | **[value](Classes/structEsMessageSliderMoved.md#variable-value)**  |
| double | **[previous](Classes/structEsMessageSliderMoved.md#variable-previous)**  |
| bool | **[inDrag](Classes/structEsMessageSliderMoved.md#variable-indrag)**  |

## Public Attributes Documentation

### variable value

```cpp
double value;
```


### variable previous

```cpp
double previous;
```


### variable inDrag

```cpp
bool inDrag;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100