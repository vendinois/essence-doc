---
title: EsStyle

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsStyleID](Files/api_8h.md#typedef-esstyleid) | **[inherit](Classes/structEsStyle.md#variable-inherit)**  |
| [EsThemeMetrics](Classes/structEsThemeMetrics.md) | **[metrics](Classes/structEsStyle.md#variable-metrics)**  |
| [EsThemeAppearance](Classes/structEsThemeAppearance.md) | **[appearance](Classes/structEsStyle.md#variable-appearance)**  |

## Public Attributes Documentation

### variable inherit

```cpp
EsStyleID inherit;
```


### variable metrics

```cpp
EsThemeMetrics metrics;
```


### variable appearance

```cpp
EsThemeAppearance appearance;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100