---
title: EsThreadEventLogEntry

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char | **[file](Classes/structEsThreadEventLogEntry.md#variable-file)**  |
| uint8_t | **[fileBytes](Classes/structEsThreadEventLogEntry.md#variable-filebytes)**  |
| char | **[expression](Classes/structEsThreadEventLogEntry.md#variable-expression)**  |
| uint8_t | **[expressionBytes](Classes/structEsThreadEventLogEntry.md#variable-expressionbytes)**  |
| uint8_t | **[event](Classes/structEsThreadEventLogEntry.md#variable-event)**  |
| uint16_t | **[line](Classes/structEsThreadEventLogEntry.md#variable-line)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[objectID](Classes/structEsThreadEventLogEntry.md#variable-objectid)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[threadID](Classes/structEsThreadEventLogEntry.md#variable-threadid)**  |

## Public Attributes Documentation

### variable file

```cpp
char file;
```


### variable fileBytes

```cpp
uint8_t fileBytes;
```


### variable expression

```cpp
char expression;
```


### variable expressionBytes

```cpp
uint8_t expressionBytes;
```


### variable event

```cpp
uint8_t event;
```


### variable line

```cpp
uint16_t line;
```


### variable objectID

```cpp
EsObjectID objectID;
```


### variable threadID

```cpp
EsObjectID threadID;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100