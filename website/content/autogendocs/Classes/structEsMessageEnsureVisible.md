---
title: EsMessageEnsureVisible

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[center](Classes/structEsMessageEnsureVisible.md#variable-center)**  |
| [EsElement](Files/api_8h.md#define-eselement) * | **[descendent](Classes/structEsMessageEnsureVisible.md#variable-descendent)**  |

## Public Attributes Documentation

### variable center

```cpp
bool center;
```


### variable descendent

```cpp
EsElement * descendent;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100