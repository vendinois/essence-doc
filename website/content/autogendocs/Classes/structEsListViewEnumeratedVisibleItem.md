---
title: EsListViewEnumeratedVisibleItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsElement](Files/api_8h.md#define-eselement) * | **[element](Classes/structEsListViewEnumeratedVisibleItem.md#variable-element)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsListViewEnumeratedVisibleItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsListViewEnumeratedVisibleItem.md#variable-index)**  |

## Public Attributes Documentation

### variable element

```cpp
EsElement * element;
```


### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100