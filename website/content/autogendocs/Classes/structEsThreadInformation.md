---
title: EsThreadInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsThreadInformation.md#variable-handle)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[tid](Classes/structEsThreadInformation.md#variable-tid)**  |

## Public Attributes Documentation

### variable handle

```cpp
EsHandle handle;
```


### variable tid

```cpp
EsObjectID tid;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100