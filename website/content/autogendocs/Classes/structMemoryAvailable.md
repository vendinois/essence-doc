---
title: MemoryAvailable

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| size_t | **[available](Classes/structMemoryAvailable.md#variable-available)**  |
| size_t | **[total](Classes/structMemoryAvailable.md#variable-total)**  |

## Public Attributes Documentation

### variable available

```cpp
size_t available;
```


### variable total

```cpp
size_t total;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100