---
title: EsMessageProcessCrash

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsCrashReason](Classes/structEsCrashReason.md) | **[reason](Classes/structEsMessageProcessCrash.md#variable-reason)**  |
| uintptr_t | **[pid](Classes/structEsMessageProcessCrash.md#variable-pid)**  |

## Public Attributes Documentation

### variable reason

```cpp
EsCrashReason reason;
```


### variable pid

```cpp
uintptr_t pid;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100