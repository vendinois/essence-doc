---
title: _EsNodeInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/struct__EsNodeInformation.md#variable-handle)**  |
| [EsFileOffset](Files/api_8h.md#typedef-esfileoffset) | **[fileSize](Classes/struct__EsNodeInformation.md#variable-filesize)**  |
| [EsFileOffsetDifference](Files/api_8h.md#typedef-esfileoffsetdifference) | **[directoryChildren](Classes/struct__EsNodeInformation.md#variable-directorychildren)**  |
| [EsNodeType](Files/api_8h.md#typedef-esnodetype) | **[type](Classes/struct__EsNodeInformation.md#variable-type)**  |

## Public Attributes Documentation

### variable handle

```cpp
EsHandle handle;
```


### variable fileSize

```cpp
EsFileOffset fileSize;
```


### variable directoryChildren

```cpp
EsFileOffsetDifference directoryChildren;
```


### variable type

```cpp
EsNodeType type;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100