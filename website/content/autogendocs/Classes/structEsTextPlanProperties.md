---
title: EsTextPlanProperties

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsCString](Files/api_8h.md#typedef-escstring) | **[cLanguage](Classes/structEsTextPlanProperties.md#variable-clanguage)**  |
| uint32_t | **[flags](Classes/structEsTextPlanProperties.md#variable-flags)**  |
| int | **[maxLines](Classes/structEsTextPlanProperties.md#variable-maxlines)**  |

## Public Attributes Documentation

### variable cLanguage

```cpp
EsCString cLanguage;
```


### variable flags

```cpp
uint32_t flags;
```


### variable maxLines

```cpp
int maxLines;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100