---
title: EsMessageAnimate

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int64_t | **[deltaMs](Classes/structEsMessageAnimate.md#variable-deltams)**  |
| int64_t | **[waitMs](Classes/structEsMessageAnimate.md#variable-waitms)**  |
| bool | **[complete](Classes/structEsMessageAnimate.md#variable-complete)**  |

## Public Attributes Documentation

### variable deltaMs

```cpp
int64_t deltaMs;
```


### variable waitMs

```cpp
int64_t waitMs;
```


### variable complete

```cpp
bool complete;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100