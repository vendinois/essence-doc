---
title: EsMessageIterateIndex

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageIterateIndex.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageIterateIndex.md#variable-index)**  |
| int64_t | **[position](Classes/structEsMessageIterateIndex.md#variable-position)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable position

```cpp
int64_t position;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100