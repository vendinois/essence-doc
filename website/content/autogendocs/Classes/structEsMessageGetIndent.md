---
title: EsMessageGetIndent

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageGetIndent.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageGetIndent.md#variable-index)**  |
| uint8_t | **[indent](Classes/structEsMessageGetIndent.md#variable-indent)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable indent

```cpp
uint8_t indent;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100