---
title: EsOpenDocumentInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[isOpen](Classes/structEsOpenDocumentInformation.md#variable-isopen)**  |
| bool | **[isModified](Classes/structEsOpenDocumentInformation.md#variable-ismodified)**  |
| uint8_t | **[applicationNameBytes](Classes/structEsOpenDocumentInformation.md#variable-applicationnamebytes)**  |
| char | **[applicationName](Classes/structEsOpenDocumentInformation.md#variable-applicationname)**  |

## Public Attributes Documentation

### variable isOpen

```cpp
bool isOpen;
```


### variable isModified

```cpp
bool isModified;
```


### variable applicationNameBytes

```cpp
uint8_t applicationNameBytes;
```


### variable applicationName

```cpp
char applicationName;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100