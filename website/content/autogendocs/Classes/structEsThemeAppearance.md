---
title: EsThemeAppearance

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[enabled](Classes/structEsThemeAppearance.md#variable-enabled)**  |
| uint32_t | **[backgroundColor](Classes/structEsThemeAppearance.md#variable-backgroundcolor)**  |
| uint32_t | **[borderColor](Classes/structEsThemeAppearance.md#variable-bordercolor)**  |
| [EsRectangle](Classes/structEsRectangle.md) | **[borderSize](Classes/structEsThemeAppearance.md#variable-bordersize)**  |

## Public Attributes Documentation

### variable enabled

```cpp
bool enabled;
```


### variable backgroundColor

```cpp
uint32_t backgroundColor;
```


### variable borderColor

```cpp
uint32_t borderColor;
```


### variable borderSize

```cpp
EsRectangle borderSize;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100