---
title: EsMessageChooseItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageChooseItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageChooseItem.md#variable-index)**  |
| uint8_t | **[source](Classes/structEsMessageChooseItem.md#variable-source)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable source

```cpp
uint8_t source;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100