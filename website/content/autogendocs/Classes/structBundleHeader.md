---
title: BundleHeader

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[signature](Classes/structBundleHeader.md#variable-signature)**  |
| uint32_t | **[version](Classes/structBundleHeader.md#variable-version)**  |
| uint32_t | **[fileCount](Classes/structBundleHeader.md#variable-filecount)**  |
| uint32_t | **[_unused](Classes/structBundleHeader.md#variable--unused)**  |
| uint64_t | **[mapAddress](Classes/structBundleHeader.md#variable-mapaddress)**  |

## Public Attributes Documentation

### variable signature

```cpp
uint32_t signature;
```


### variable version

```cpp
uint32_t version;
```


### variable fileCount

```cpp
uint32_t fileCount;
```


### variable _unused

```cpp
uint32_t _unused;
```


### variable mapAddress

```cpp
uint64_t mapAddress;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100