---
title: EsMessageCreateInstance

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[window](Classes/structEsMessageCreateInstance.md#variable-window)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[data](Classes/structEsMessageCreateInstance.md#variable-data)**  |
| size_t | **[dataBytes](Classes/structEsMessageCreateInstance.md#variable-databytes)**  |

## Public Attributes Documentation

### variable window

```cpp
EsHandle window;
```


### variable data

```cpp
EsHandle data;
```


### variable dataBytes

```cpp
size_t dataBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100