---
title: EsMessageScroll

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[scroll](Classes/structEsMessageScroll.md#variable-scroll)**  |
| int | **[previous](Classes/structEsMessageScroll.md#variable-previous)**  |

## Public Attributes Documentation

### variable scroll

```cpp
int scroll;
```


### variable previous

```cpp
int previous;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100