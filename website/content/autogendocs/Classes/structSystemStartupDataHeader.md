---
title: SystemStartupDataHeader

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| size_t | **[initialMountPointCount](Classes/structSystemStartupDataHeader.md#variable-initialmountpointcount)**  |
| size_t | **[initialDeviceCount](Classes/structSystemStartupDataHeader.md#variable-initialdevicecount)**  |
| uintptr_t | **[themeCursorData](Classes/structSystemStartupDataHeader.md#variable-themecursordata)**  |
| uintptr_t | **[desktopRequestPipe](Classes/structSystemStartupDataHeader.md#variable-desktoprequestpipe)**  |
| uintptr_t | **[desktopResponsePipe](Classes/structSystemStartupDataHeader.md#variable-desktopresponsepipe)**  |

## Public Attributes Documentation

### variable initialMountPointCount

```cpp
size_t initialMountPointCount;
```


### variable initialDeviceCount

```cpp
size_t initialDeviceCount;
```


### variable themeCursorData

```cpp
uintptr_t themeCursorData;
```


### variable desktopRequestPipe

```cpp
uintptr_t desktopRequestPipe;
```


### variable desktopResponsePipe

```cpp
uintptr_t desktopResponsePipe;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100