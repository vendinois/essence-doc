---
title: EsMessageZOrder

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uintptr_t | **[index](Classes/structEsMessageZOrder.md#variable-index)**  |
| [EsElement](Files/api_8h.md#define-eselement) * | **[child](Classes/structEsMessageZOrder.md#variable-child)**  |

## Public Attributes Documentation

### variable index

```cpp
uintptr_t index;
```


### variable child

```cpp
EsElement * child;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100