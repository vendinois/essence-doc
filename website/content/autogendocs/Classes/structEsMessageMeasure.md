---
title: EsMessageMeasure

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[width](Classes/structEsMessageMeasure.md#variable-width)**  |
| int | **[height](Classes/structEsMessageMeasure.md#variable-height)**  |
| bool | **[internalMeasurement](Classes/structEsMessageMeasure.md#variable-internalmeasurement)**  |

## Public Attributes Documentation

### variable width

```cpp
int width;
```


### variable height

```cpp
int height;
```


### variable internalMeasurement

```cpp
bool internalMeasurement;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100