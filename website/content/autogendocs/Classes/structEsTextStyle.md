---
title: EsTextStyle

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsFont](Classes/structEsFont.md) | **[font](Classes/structEsTextStyle.md#variable-font)**  |
| uint16_t | **[size](Classes/structEsTextStyle.md#variable-size)**  |
| uint16_t | **[baselineOffset](Classes/structEsTextStyle.md#variable-baselineoffset)**  |
| int8_t | **[tracking](Classes/structEsTextStyle.md#variable-tracking)**  |
| uint8_t | **[figures](Classes/structEsTextStyle.md#variable-figures)**  |
| bool | **[alternateDirection](Classes/structEsTextStyle.md#variable-alternatedirection)**  |
| uint8_t | **[blur](Classes/structEsTextStyle.md#variable-blur)**  |
| uint8_t | **[decorations](Classes/structEsTextStyle.md#variable-decorations)**  |
| uint32_t | **[color](Classes/structEsTextStyle.md#variable-color)**  |
| uint32_t | **[decorationsColor](Classes/structEsTextStyle.md#variable-decorationscolor)**  |

## Public Attributes Documentation

### variable font

```cpp
EsFont font;
```


### variable size

```cpp
uint16_t size;
```


### variable baselineOffset

```cpp
uint16_t baselineOffset;
```


### variable tracking

```cpp
int8_t tracking;
```


### variable figures

```cpp
uint8_t figures;
```


### variable alternateDirection

```cpp
bool alternateDirection;
```


### variable blur

```cpp
uint8_t blur;
```


### variable decorations

```cpp
uint8_t decorations;
```


### variable color

```cpp
uint32_t color;
```


### variable decorationsColor

```cpp
uint32_t decorationsColor;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100