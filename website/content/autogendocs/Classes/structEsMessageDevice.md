---
title: EsMessageDevice

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[id](Classes/structEsMessageDevice.md#variable-id)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsMessageDevice.md#variable-handle)**  |
| [EsDeviceType](Files/api_8h.md#enum-esdevicetype) | **[type](Classes/structEsMessageDevice.md#variable-type)**  |

## Public Attributes Documentation

### variable id

```cpp
EsObjectID id;
```


### variable handle

```cpp
EsHandle handle;
```


### variable type

```cpp
EsDeviceType type;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100