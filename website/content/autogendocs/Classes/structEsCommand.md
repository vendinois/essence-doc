---
title: EsCommand

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsElement](Files/api_8h.md#define-eselement) ** | **[elements](Classes/structEsCommand.md#variable-elements)**  |
| [EsCommandCallback](Files/api_8h.md#typedef-escommandcallback) | **[callback](Classes/structEsCommand.md#variable-callback)**  |
| bool | **[enabled](Classes/structEsCommand.md#variable-enabled)**  |
| bool | **[registered](Classes/structEsCommand.md#variable-registered)**  |
| bool | **[allocated](Classes/structEsCommand.md#variable-allocated)**  |
| [EsCheckState](Files/api_8h.md#enum-escheckstate) | **[check](Classes/structEsCommand.md#variable-check)**  |
| uint32_t | **[stableID](Classes/structEsCommand.md#variable-stableid)**  |
| [EsCString](Files/api_8h.md#typedef-escstring) | **[cKeyboardShortcut](Classes/structEsCommand.md#variable-ckeyboardshortcut)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[data](Classes/structEsCommand.md#variable-data)**  |
| const char * | **[title](Classes/structEsCommand.md#variable-title)**  |
| ptrdiff_t | **[titleBytes](Classes/structEsCommand.md#variable-titlebytes)**  |

## Public Attributes Documentation

### variable elements

```cpp
EsElement ** elements;
```


### variable callback

```cpp
EsCommandCallback callback;
```


### variable enabled

```cpp
bool enabled;
```


### variable registered

```cpp
bool registered;
```


### variable allocated

```cpp
bool allocated;
```


### variable check

```cpp
EsCheckState check;
```


### variable stableID

```cpp
uint32_t stableID;
```


### variable cKeyboardShortcut

```cpp
EsCString cKeyboardShortcut;
```


### variable data

```cpp
EsGeneric data;
```


### variable title

```cpp
const char * title;
```


### variable titleBytes

```cpp
ptrdiff_t titleBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100