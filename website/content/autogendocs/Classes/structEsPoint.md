---
title: EsPoint

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int32_t | **[x](Classes/structEsPoint.md#variable-x)**  |
| int32_t | **[y](Classes/structEsPoint.md#variable-y)**  |

## Public Attributes Documentation

### variable x

```cpp
int32_t x;
```


### variable y

```cpp
int32_t y;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100