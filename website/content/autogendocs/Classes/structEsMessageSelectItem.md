---
title: EsMessageSelectItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageSelectItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageSelectItem.md#variable-index)**  |
| bool | **[isSelected](Classes/structEsMessageSelectItem.md#variable-isselected)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable isSelected

```cpp
bool isSelected;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100