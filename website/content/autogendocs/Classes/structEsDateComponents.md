---
title: EsDateComponents

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint16_t | **[year](Classes/structEsDateComponents.md#variable-year)**  |
| uint8_t | **[month](Classes/structEsDateComponents.md#variable-month)**  |
| uint8_t | **[day](Classes/structEsDateComponents.md#variable-day)**  |
| uint8_t | **[hour](Classes/structEsDateComponents.md#variable-hour)**  |
| uint8_t | **[minute](Classes/structEsDateComponents.md#variable-minute)**  |
| uint8_t | **[second](Classes/structEsDateComponents.md#variable-second)**  |
| uint8_t | **[_unused](Classes/structEsDateComponents.md#variable--unused)**  |
| uint16_t | **[millisecond](Classes/structEsDateComponents.md#variable-millisecond)**  |

## Public Attributes Documentation

### variable year

```cpp
uint16_t year;
```


### variable month

```cpp
uint8_t month;
```


### variable day

```cpp
uint8_t day;
```


### variable hour

```cpp
uint8_t hour;
```


### variable minute

```cpp
uint8_t minute;
```


### variable second

```cpp
uint8_t second;
```


### variable _unused

```cpp
uint8_t _unused;
```


### variable millisecond

```cpp
uint16_t millisecond;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100