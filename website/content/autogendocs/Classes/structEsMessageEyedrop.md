---
title: EsMessageEyedrop

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[color](Classes/structEsMessageEyedrop.md#variable-color)**  |
| bool | **[cancelled](Classes/structEsMessageEyedrop.md#variable-cancelled)**  |

## Public Attributes Documentation

### variable color

```cpp
uint32_t color;
```


### variable cancelled

```cpp
bool cancelled;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100