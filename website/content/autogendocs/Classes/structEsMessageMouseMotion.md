---
title: EsMessageMouseMotion

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[newPositionX](Classes/structEsMessageMouseMotion.md#variable-newpositionx)**  |
| int | **[newPositionY](Classes/structEsMessageMouseMotion.md#variable-newpositiony)**  |
| int | **[originalPositionX](Classes/structEsMessageMouseMotion.md#variable-originalpositionx)**  |
| int | **[originalPositionY](Classes/structEsMessageMouseMotion.md#variable-originalpositiony)**  |

## Public Attributes Documentation

### variable newPositionX

```cpp
int newPositionX;
```


### variable newPositionY

```cpp
int newPositionY;
```


### variable originalPositionX

```cpp
int originalPositionX;
```


### variable originalPositionY

```cpp
int originalPositionY;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100