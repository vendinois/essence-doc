---
title: EsMessageNumberUpdated

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| double | **[delta](Classes/structEsMessageNumberUpdated.md#variable-delta)**  |
| double | **[newValue](Classes/structEsMessageNumberUpdated.md#variable-newvalue)**  |

## Public Attributes Documentation

### variable delta

```cpp
double delta;
```


### variable newValue

```cpp
double newValue;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100