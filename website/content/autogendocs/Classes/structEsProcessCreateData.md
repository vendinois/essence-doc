---
title: EsProcessCreateData

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[systemData](Classes/structEsProcessCreateData.md#variable-systemdata)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[subsystemData](Classes/structEsProcessCreateData.md#variable-subsystemdata)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[userData](Classes/structEsProcessCreateData.md#variable-userdata)**  |
| uint8_t | **[subsystemID](Classes/structEsProcessCreateData.md#variable-subsystemid)**  |

## Public Attributes Documentation

### variable systemData

```cpp
EsHandle systemData;
```


### variable subsystemData

```cpp
EsHandle subsystemData;
```


### variable userData

```cpp
EsGeneric userData;
```


### variable subsystemID

```cpp
uint8_t subsystemID;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100