---
title: EsMessageLayout

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[sizeChanged](Classes/structEsMessageLayout.md#variable-sizechanged)**  |

## Public Attributes Documentation

### variable sizeChanged

```cpp
bool sizeChanged;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100