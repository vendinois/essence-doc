---
title: EsCrashReason

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsFatalError](Files/api_8h.md#enum-esfatalerror) | **[errorCode](Classes/structEsCrashReason.md#variable-errorcode)**  |
| int32_t | **[duringSystemCall](Classes/structEsCrashReason.md#variable-duringsystemcall)**  |

## Public Attributes Documentation

### variable errorCode

```cpp
EsFatalError errorCode;
```


### variable duringSystemCall

```cpp
int32_t duringSystemCall;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100