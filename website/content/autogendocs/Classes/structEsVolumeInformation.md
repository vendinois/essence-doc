---
title: EsVolumeInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char | **[label](Classes/structEsVolumeInformation.md#variable-label)**  |
| uint8_t | **[labelBytes](Classes/structEsVolumeInformation.md#variable-labelbytes)**  |
| uint8_t | **[driveType](Classes/structEsVolumeInformation.md#variable-drivetype)**  |
| uint32_t | **[flags](Classes/structEsVolumeInformation.md#variable-flags)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[id](Classes/structEsVolumeInformation.md#variable-id)**  |
| [EsFileOffset](Files/api_8h.md#typedef-esfileoffset) | **[spaceTotal](Classes/structEsVolumeInformation.md#variable-spacetotal)**  |
| [EsFileOffset](Files/api_8h.md#typedef-esfileoffset) | **[spaceUsed](Classes/structEsVolumeInformation.md#variable-spaceused)**  |
| [EsUniqueIdentifier](Classes/structEsUniqueIdentifier.md) | **[identifier](Classes/structEsVolumeInformation.md#variable-identifier)**  |
| [EsUniqueIdentifier](Classes/structEsUniqueIdentifier.md) | **[installationIdentifier](Classes/structEsVolumeInformation.md#variable-installationidentifier)**  |

## Public Attributes Documentation

### variable label

```cpp
char label;
```


### variable labelBytes

```cpp
uint8_t labelBytes;
```


### variable driveType

```cpp
uint8_t driveType;
```


### variable flags

```cpp
uint32_t flags;
```


### variable id

```cpp
EsObjectID id;
```


### variable spaceTotal

```cpp
EsFileOffset spaceTotal;
```


### variable spaceUsed

```cpp
EsFileOffset spaceUsed;
```


### variable identifier

```cpp
EsUniqueIdentifier identifier;
```


### variable installationIdentifier

```cpp
EsUniqueIdentifier installationIdentifier;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100