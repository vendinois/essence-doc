---
title: EsMessageNumberDragDelta

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[delta](Classes/structEsMessageNumberDragDelta.md#variable-delta)**  |
| int32_t | **[hoverCharacter](Classes/structEsMessageNumberDragDelta.md#variable-hovercharacter)**  |
| bool | **[fast](Classes/structEsMessageNumberDragDelta.md#variable-fast)**  |

## Public Attributes Documentation

### variable delta

```cpp
int delta;
```


### variable hoverCharacter

```cpp
int32_t hoverCharacter;
```


### variable fast

```cpp
bool fast;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100