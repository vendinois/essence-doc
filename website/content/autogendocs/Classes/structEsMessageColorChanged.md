---
title: EsMessageColorChanged

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[newColor](Classes/structEsMessageColorChanged.md#variable-newcolor)**  |
| bool | **[pickerClosed](Classes/structEsMessageColorChanged.md#variable-pickerclosed)**  |

## Public Attributes Documentation

### variable newColor

```cpp
uint32_t newColor;
```


### variable pickerClosed

```cpp
bool pickerClosed;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100