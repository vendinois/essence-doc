---
title: EsMessageScrollWheel

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int32_t | **[dx](Classes/structEsMessageScrollWheel.md#variable-dx)**  |
| int32_t | **[dy](Classes/structEsMessageScrollWheel.md#variable-dy)**  |

## Public Attributes Documentation

### variable dx

```cpp
int32_t dx;
```


### variable dy

```cpp
int32_t dy;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100