---
title: EsRectangle

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int32_t | **[l](Classes/structEsRectangle.md#variable-l)**  |
| int32_t | **[r](Classes/structEsRectangle.md#variable-r)**  |
| int32_t | **[t](Classes/structEsRectangle.md#variable-t)**  |
| int32_t | **[b](Classes/structEsRectangle.md#variable-b)**  |

## Public Attributes Documentation

### variable l

```cpp
int32_t l;
```


### variable r

```cpp
int32_t r;
```


### variable t

```cpp
int32_t t;
```


### variable b

```cpp
int32_t b;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100