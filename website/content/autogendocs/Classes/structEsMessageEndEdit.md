---
title: EsMessageEndEdit

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[rejected](Classes/structEsMessageEndEdit.md#variable-rejected)**  |
| bool | **[unchanged](Classes/structEsMessageEndEdit.md#variable-unchanged)**  |

## Public Attributes Documentation

### variable rejected

```cpp
bool rejected;
```


### variable unchanged

```cpp
bool unchanged;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100