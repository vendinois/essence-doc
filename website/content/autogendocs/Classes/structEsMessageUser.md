---
title: EsMessageUser

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsGeneric](Classes/unionEsGeneric.md) | **[context1](Classes/structEsMessageUser.md#variable-context1)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[context2](Classes/structEsMessageUser.md#variable-context2)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[context3](Classes/structEsMessageUser.md#variable-context3)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[context4](Classes/structEsMessageUser.md#variable-context4)**  |

## Public Attributes Documentation

### variable context1

```cpp
EsGeneric context1;
```


### variable context2

```cpp
EsGeneric context2;
```


### variable context3

```cpp
EsGeneric context3;
```


### variable context4

```cpp
EsGeneric context4;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100