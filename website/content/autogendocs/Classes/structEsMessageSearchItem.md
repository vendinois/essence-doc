---
title: EsMessageSearchItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageSearchItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageSearchItem.md#variable-index)**  |
| const char * | **[query](Classes/structEsMessageSearchItem.md#variable-query)**  |
| ptrdiff_t | **[queryBytes](Classes/structEsMessageSearchItem.md#variable-querybytes)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable query

```cpp
const char * query;
```


### variable queryBytes

```cpp
ptrdiff_t queryBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100