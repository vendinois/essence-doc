---
title: EsPanelBandDecorator

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uintptr_t | **[index](Classes/structEsPanelBandDecorator.md#variable-index)**  |
| uintptr_t | **[repeatEvery](Classes/structEsPanelBandDecorator.md#variable-repeatevery)**  |
| uint8_t | **[axis](Classes/structEsPanelBandDecorator.md#variable-axis)**  |
| [EsThemeAppearance](Classes/structEsThemeAppearance.md) | **[appearance](Classes/structEsPanelBandDecorator.md#variable-appearance)**  |

## Public Attributes Documentation

### variable index

```cpp
uintptr_t index;
```


### variable repeatEvery

```cpp
uintptr_t repeatEvery;
```


### variable axis

```cpp
uint8_t axis;
```


### variable appearance

```cpp
EsThemeAppearance appearance;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100