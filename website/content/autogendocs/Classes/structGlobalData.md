---
title: GlobalData

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| volatile int32_t | **[clickChainTimeoutMs](Classes/structGlobalData.md#variable-clickchaintimeoutms)**  |
| volatile float | **[uiScale](Classes/structGlobalData.md#variable-uiscale)**  |
| volatile bool | **[swapLeftAndRightButtons](Classes/structGlobalData.md#variable-swapleftandrightbuttons)**  |
| volatile bool | **[showCursorShadow](Classes/structGlobalData.md#variable-showcursorshadow)**  |
| volatile bool | **[useSmartQuotes](Classes/structGlobalData.md#variable-usesmartquotes)**  |
| volatile bool | **[enableHoverState](Classes/structGlobalData.md#variable-enablehoverstate)**  |
| volatile float | **[animationTimeMultiplier](Classes/structGlobalData.md#variable-animationtimemultiplier)**  |
| volatile uint64_t | **[schedulerTimeMs](Classes/structGlobalData.md#variable-schedulertimems)**  |
| volatile uint64_t | **[schedulerTimeOffset](Classes/structGlobalData.md#variable-schedulertimeoffset)**  |
| volatile uint16_t | **[keyboardLayout](Classes/structGlobalData.md#variable-keyboardlayout)**  |

## Public Attributes Documentation

### variable clickChainTimeoutMs

```cpp
volatile int32_t clickChainTimeoutMs;
```


### variable uiScale

```cpp
volatile float uiScale;
```


### variable swapLeftAndRightButtons

```cpp
volatile bool swapLeftAndRightButtons;
```


### variable showCursorShadow

```cpp
volatile bool showCursorShadow;
```


### variable useSmartQuotes

```cpp
volatile bool useSmartQuotes;
```


### variable enableHoverState

```cpp
volatile bool enableHoverState;
```


### variable animationTimeMultiplier

```cpp
volatile float animationTimeMultiplier;
```


### variable schedulerTimeMs

```cpp
volatile uint64_t schedulerTimeMs;
```


### variable schedulerTimeOffset

```cpp
volatile uint64_t schedulerTimeOffset;
```


### variable keyboardLayout

```cpp
volatile uint16_t keyboardLayout;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100