---
title: EsGeneric

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uintptr_t | **[u](Classes/unionEsGeneric.md#variable-u)**  |
| intptr_t | **[i](Classes/unionEsGeneric.md#variable-i)**  |
| void * | **[p](Classes/unionEsGeneric.md#variable-p)**  |

## Public Attributes Documentation

### variable u

```cpp
uintptr_t u;
```


### variable i

```cpp
intptr_t i;
```


### variable p

```cpp
void * p;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100