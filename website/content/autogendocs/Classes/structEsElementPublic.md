---
title: EsElementPublic

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsElementCallback](Files/api_8h.md#typedef-eselementcallback) | **[messageUser](Classes/structEsElementPublic.md#variable-messageuser)**  |
| [EsCString](Files/api_8h.md#typedef-escstring) | **[cName](Classes/structEsElementPublic.md#variable-cname)**  |
| [EsGeneric](Classes/unionEsGeneric.md) | **[userData](Classes/structEsElementPublic.md#variable-userdata)**  |
| char | **[accessKey](Classes/structEsElementPublic.md#variable-accesskey)**  |
| [EsWindow](Files/api_8h.md#define-eswindow) * | **[window](Classes/structEsElementPublic.md#variable-window)**  |
| [ES_INSTANCE_TYPE](Files/api_8h.md#define-es-instance-type) * | **[instance](Classes/structEsElementPublic.md#variable-instance)**  |
| uint64_t | **[flags](Classes/structEsElementPublic.md#variable-flags)**  |

## Public Attributes Documentation

### variable messageUser

```cpp
EsElementCallback messageUser;
```


### variable cName

```cpp
EsCString cName;
```


### variable userData

```cpp
EsGeneric userData;
```


### variable accessKey

```cpp
char accessKey;
```


### variable window

```cpp
EsWindow * window;
```


### variable instance

```cpp
ES_INSTANCE_TYPE * instance;
```


### variable flags

```cpp
uint64_t flags;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100