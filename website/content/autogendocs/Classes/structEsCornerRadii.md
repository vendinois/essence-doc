---
title: EsCornerRadii

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[tl](Classes/structEsCornerRadii.md#variable-tl)**  |
| uint32_t | **[tr](Classes/structEsCornerRadii.md#variable-tr)**  |
| uint32_t | **[bl](Classes/structEsCornerRadii.md#variable-bl)**  |
| uint32_t | **[br](Classes/structEsCornerRadii.md#variable-br)**  |

## Public Attributes Documentation

### variable tl

```cpp
uint32_t tl;
```


### variable tr

```cpp
uint32_t tr;
```


### variable bl

```cpp
uint32_t bl;
```


### variable br

```cpp
uint32_t br;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100