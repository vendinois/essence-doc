---
title: EsMessageFocus

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[flags](Classes/structEsMessageFocus.md#variable-flags)**  |

## Public Attributes Documentation

### variable flags

```cpp
uint32_t flags;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100