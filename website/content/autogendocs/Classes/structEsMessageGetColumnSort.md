---
title: EsMessageGetColumnSort

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint8_t | **[index](Classes/structEsMessageGetColumnSort.md#variable-index)**  |

## Public Attributes Documentation

### variable index

```cpp
uint8_t index;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100