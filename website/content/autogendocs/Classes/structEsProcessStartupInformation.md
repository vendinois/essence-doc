---
title: EsProcessStartupInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[isDesktop](Classes/structEsProcessStartupInformation.md#variable-isdesktop)**  |
| bool | **[isBundle](Classes/structEsProcessStartupInformation.md#variable-isbundle)**  |
| uintptr_t | **[applicationStartAddress](Classes/structEsProcessStartupInformation.md#variable-applicationstartaddress)**  |
| uintptr_t | **[tlsImageStart](Classes/structEsProcessStartupInformation.md#variable-tlsimagestart)**  |
| uintptr_t | **[tlsImageBytes](Classes/structEsProcessStartupInformation.md#variable-tlsimagebytes)**  |
| uintptr_t | **[tlsBytes](Classes/structEsProcessStartupInformation.md#variable-tlsbytes)**  |
| uintptr_t | **[timeStampTicksPerMs](Classes/structEsProcessStartupInformation.md#variable-timestampticksperms)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[globalDataRegion](Classes/structEsProcessStartupInformation.md#variable-globaldataregion)**  |
| [EsProcessCreateData](Classes/structEsProcessCreateData.md) | **[data](Classes/structEsProcessStartupInformation.md#variable-data)**  |

## Public Attributes Documentation

### variable isDesktop

```cpp
bool isDesktop;
```


### variable isBundle

```cpp
bool isBundle;
```


### variable applicationStartAddress

```cpp
uintptr_t applicationStartAddress;
```


### variable tlsImageStart

```cpp
uintptr_t tlsImageStart;
```


### variable tlsImageBytes

```cpp
uintptr_t tlsImageBytes;
```


### variable tlsBytes

```cpp
uintptr_t tlsBytes;
```


### variable timeStampTicksPerMs

```cpp
uintptr_t timeStampTicksPerMs;
```


### variable globalDataRegion

```cpp
EsHandle globalDataRegion;
```


### variable data

```cpp
EsProcessCreateData data;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100