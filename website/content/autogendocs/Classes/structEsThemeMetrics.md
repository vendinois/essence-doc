---
title: EsThemeMetrics

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint64_t | **[mask](Classes/structEsThemeMetrics.md#variable-mask)**  |
| [EsRectangle](Classes/structEsRectangle.md) | **[insets](Classes/structEsThemeMetrics.md#variable-insets)**  |
| [EsRectangle](Classes/structEsRectangle.md) | **[clipInsets](Classes/structEsThemeMetrics.md#variable-clipinsets)**  |
| bool | **[clipEnabled](Classes/structEsThemeMetrics.md#variable-clipenabled)**  |
| [EsCursorStyle](Files/api_8h.md#enum-escursorstyle) | **[cursor](Classes/structEsThemeMetrics.md#variable-cursor)**  |
| int32_t | **[preferredWidth](Classes/structEsThemeMetrics.md#variable-preferredwidth)**  |
| int32_t | **[preferredHeight](Classes/structEsThemeMetrics.md#variable-preferredheight)**  |
| int32_t | **[minimumWidth](Classes/structEsThemeMetrics.md#variable-minimumwidth)**  |
| int32_t | **[minimumHeight](Classes/structEsThemeMetrics.md#variable-minimumheight)**  |
| int32_t | **[maximumWidth](Classes/structEsThemeMetrics.md#variable-maximumwidth)**  |
| int32_t | **[maximumHeight](Classes/structEsThemeMetrics.md#variable-maximumheight)**  |
| int32_t | **[gapMajor](Classes/structEsThemeMetrics.md#variable-gapmajor)**  |
| int32_t | **[gapMinor](Classes/structEsThemeMetrics.md#variable-gapminor)**  |
| int32_t | **[gapWrap](Classes/structEsThemeMetrics.md#variable-gapwrap)**  |
| uint32_t | **[textColor](Classes/structEsThemeMetrics.md#variable-textcolor)**  |
| uint32_t | **[selectedBackground](Classes/structEsThemeMetrics.md#variable-selectedbackground)**  |
| uint32_t | **[selectedText](Classes/structEsThemeMetrics.md#variable-selectedtext)**  |
| uint32_t | **[iconColor](Classes/structEsThemeMetrics.md#variable-iconcolor)**  |
| int32_t | **[textAlign](Classes/structEsThemeMetrics.md#variable-textalign)**  |
| int32_t | **[textSize](Classes/structEsThemeMetrics.md#variable-textsize)**  |
| int32_t | **[fontFamily](Classes/structEsThemeMetrics.md#variable-fontfamily)**  |
| int32_t | **[fontWeight](Classes/structEsThemeMetrics.md#variable-fontweight)**  |
| int32_t | **[iconSize](Classes/structEsThemeMetrics.md#variable-iconsize)**  |
| uint8_t | **[textFigures](Classes/structEsThemeMetrics.md#variable-textfigures)**  |
| bool | **[isItalic](Classes/structEsThemeMetrics.md#variable-isitalic)**  |
| bool | **[layoutVertical](Classes/structEsThemeMetrics.md#variable-layoutvertical)**  |

## Public Attributes Documentation

### variable mask

```cpp
uint64_t mask;
```


### variable insets

```cpp
EsRectangle insets;
```


### variable clipInsets

```cpp
EsRectangle clipInsets;
```


### variable clipEnabled

```cpp
bool clipEnabled;
```


### variable cursor

```cpp
EsCursorStyle cursor;
```


### variable preferredWidth

```cpp
int32_t preferredWidth;
```


### variable preferredHeight

```cpp
int32_t preferredHeight;
```


### variable minimumWidth

```cpp
int32_t minimumWidth;
```


### variable minimumHeight

```cpp
int32_t minimumHeight;
```


### variable maximumWidth

```cpp
int32_t maximumWidth;
```


### variable maximumHeight

```cpp
int32_t maximumHeight;
```


### variable gapMajor

```cpp
int32_t gapMajor;
```


### variable gapMinor

```cpp
int32_t gapMinor;
```


### variable gapWrap

```cpp
int32_t gapWrap;
```


### variable textColor

```cpp
uint32_t textColor;
```


### variable selectedBackground

```cpp
uint32_t selectedBackground;
```


### variable selectedText

```cpp
uint32_t selectedText;
```


### variable iconColor

```cpp
uint32_t iconColor;
```


### variable textAlign

```cpp
int32_t textAlign;
```


### variable textSize

```cpp
int32_t textSize;
```


### variable fontFamily

```cpp
int32_t fontFamily;
```


### variable fontWeight

```cpp
int32_t fontWeight;
```


### variable iconSize

```cpp
int32_t iconSize;
```


### variable textFigures

```cpp
uint8_t textFigures;
```


### variable isItalic

```cpp
bool isItalic;
```


### variable layoutVertical

```cpp
bool layoutVertical;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100