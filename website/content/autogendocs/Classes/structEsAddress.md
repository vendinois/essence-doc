---
title: EsAddress

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[ipv4](Classes/structEsAddress.md#variable-ipv4)**  |
| uint16_t | **[port](Classes/structEsAddress.md#variable-port)**  |
| uint8_t | **[d](Classes/structEsAddress.md#variable-d)**  |
| union EsAddress::@4 | **[@5](Classes/structEsAddress.md#variable-@5)**  |

## Public Attributes Documentation

### variable ipv4

```cpp
uint32_t ipv4;
```


### variable port

```cpp
uint16_t port;
```


### variable d

```cpp
uint8_t d;
```


### variable @5

```cpp
union EsAddress::@4 @5;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100