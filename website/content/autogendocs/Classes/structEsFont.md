---
title: EsFont

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsFontFamily](Files/api_8h.md#typedef-esfontfamily) | **[family](Classes/structEsFont.md#variable-family)**  |
| uint8_t | **[weight](Classes/structEsFont.md#variable-weight)**  |
| uint8_t | **[flags](Classes/structEsFont.md#variable-flags)**  |

## Public Attributes Documentation

### variable family

```cpp
EsFontFamily family;
```


### variable weight

```cpp
uint8_t weight;
```


### variable flags

```cpp
uint8_t flags;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100