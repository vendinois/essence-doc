---
title: EsInstanceClassEditorSettings

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| const char * | **[newDocumentFileName](Classes/structEsInstanceClassEditorSettings.md#variable-newdocumentfilename)**  |
| ptrdiff_t | **[newDocumentFileNameBytes](Classes/structEsInstanceClassEditorSettings.md#variable-newdocumentfilenamebytes)**  |
| const char * | **[newDocumentTitle](Classes/structEsInstanceClassEditorSettings.md#variable-newdocumenttitle)**  |
| ptrdiff_t | **[newDocumentTitleBytes](Classes/structEsInstanceClassEditorSettings.md#variable-newdocumenttitlebytes)**  |
| uint32_t | **[documentIconID](Classes/structEsInstanceClassEditorSettings.md#variable-documenticonid)**  |

## Public Attributes Documentation

### variable newDocumentFileName

```cpp
const char * newDocumentFileName;
```


### variable newDocumentFileNameBytes

```cpp
ptrdiff_t newDocumentFileNameBytes;
```


### variable newDocumentTitle

```cpp
const char * newDocumentTitle;
```


### variable newDocumentTitleBytes

```cpp
ptrdiff_t newDocumentTitleBytes;
```


### variable documentIconID

```cpp
uint32_t documentIconID;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100