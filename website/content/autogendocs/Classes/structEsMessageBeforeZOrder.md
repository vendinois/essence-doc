---
title: EsMessageBeforeZOrder

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uintptr_t | **[start](Classes/structEsMessageBeforeZOrder.md#variable-start)**  |
| uintptr_t | **[end](Classes/structEsMessageBeforeZOrder.md#variable-end)**  |
| uintptr_t | **[nonClient](Classes/structEsMessageBeforeZOrder.md#variable-nonclient)**  |
| [EsRectangle](Classes/structEsRectangle.md) | **[clip](Classes/structEsMessageBeforeZOrder.md#variable-clip)**  |

## Public Attributes Documentation

### variable start

```cpp
uintptr_t start;
```


### variable end

```cpp
uintptr_t end;
```


### variable nonClient

```cpp
uintptr_t nonClient;
```


### variable clip

```cpp
EsRectangle clip;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100