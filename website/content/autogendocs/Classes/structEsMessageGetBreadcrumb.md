---
title: EsMessageGetBreadcrumb

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uintptr_t | **[index](Classes/structEsMessageGetBreadcrumb.md#variable-index)**  |
| [EsBuffer](Classes/structEsBuffer.md) * | **[buffer](Classes/structEsMessageGetBreadcrumb.md#variable-buffer)**  |
| uint32_t | **[icon](Classes/structEsMessageGetBreadcrumb.md#variable-icon)**  |

## Public Attributes Documentation

### variable index

```cpp
uintptr_t index;
```


### variable buffer

```cpp
EsBuffer * buffer;
```


### variable icon

```cpp
uint32_t icon;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100