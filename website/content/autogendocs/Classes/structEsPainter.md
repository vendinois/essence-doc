---
title: EsPainter

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsRectangle](Classes/structEsRectangle.md) | **[clip](Classes/structEsPainter.md#variable-clip)**  |
| int32_t | **[offsetX](Classes/structEsPainter.md#variable-offsetx)**  |
| int32_t | **[offsetY](Classes/structEsPainter.md#variable-offsety)**  |
| int32_t | **[width](Classes/structEsPainter.md#variable-width)**  |
| int32_t | **[height](Classes/structEsPainter.md#variable-height)**  |
| void * | **[style](Classes/structEsPainter.md#variable-style)**  |
| [EsPaintTarget](Files/api_8h.md#define-espainttarget) * | **[target](Classes/structEsPainter.md#variable-target)**  |

## Public Attributes Documentation

### variable clip

```cpp
EsRectangle clip;
```


### variable offsetX

```cpp
int32_t offsetX;
```


### variable offsetY

```cpp
int32_t offsetY;
```


### variable width

```cpp
int32_t width;
```


### variable height

```cpp
int32_t height;
```


### variable style

```cpp
void * style;
```


### variable target

```cpp
EsPaintTarget * target;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100