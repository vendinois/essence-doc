---
title: EsMessageCreateItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageCreateItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageCreateItem.md#variable-index)**  |
| [EsElement](Files/api_8h.md#define-eselement) * | **[item](Classes/structEsMessageCreateItem.md#variable-item)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable item

```cpp
EsElement * item;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100