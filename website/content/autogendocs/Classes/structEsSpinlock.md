---
title: EsSpinlock

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| volatile uint8_t | **[state](Classes/structEsSpinlock.md#variable-state)**  |

## Public Attributes Documentation

### variable state

```cpp
volatile uint8_t state;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100