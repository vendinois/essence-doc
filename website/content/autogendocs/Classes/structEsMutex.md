---
title: EsMutex

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[event](Classes/structEsMutex.md#variable-event)**  |
| [EsSpinlock](Classes/structEsSpinlock.md) | **[spinlock](Classes/structEsMutex.md#variable-spinlock)**  |
| volatile uint8_t | **[state](Classes/structEsMutex.md#variable-state)**  |
| volatile uint32_t | **[queued](Classes/structEsMutex.md#variable-queued)**  |

## Public Attributes Documentation

### variable event

```cpp
EsHandle event;
```


### variable spinlock

```cpp
EsSpinlock spinlock;
```


### variable state

```cpp
volatile uint8_t state;
```


### variable queued

```cpp
volatile uint32_t queued;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100