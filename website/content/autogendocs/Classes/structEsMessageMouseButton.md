---
title: EsMessageMouseButton

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[positionX](Classes/structEsMessageMouseButton.md#variable-positionx)**  |
| int | **[positionY](Classes/structEsMessageMouseButton.md#variable-positiony)**  |
| uint8_t | **[clickChainCount](Classes/structEsMessageMouseButton.md#variable-clickchaincount)**  |

## Public Attributes Documentation

### variable positionX

```cpp
int positionX;
```


### variable positionY

```cpp
int positionY;
```


### variable clickChainCount

```cpp
uint8_t clickChainCount;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100