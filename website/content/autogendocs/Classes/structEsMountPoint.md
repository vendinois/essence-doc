---
title: EsMountPoint

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char | **[prefix](Classes/structEsMountPoint.md#variable-prefix)**  |
| size_t | **[prefixBytes](Classes/structEsMountPoint.md#variable-prefixbytes)**  |
| uintptr_t | **[base](Classes/structEsMountPoint.md#variable-base)**  |
| bool | **[addedByApplication](Classes/structEsMountPoint.md#variable-addedbyapplication)**  |

## Public Attributes Documentation

### variable prefix

```cpp
char prefix;
```


### variable prefixBytes

```cpp
size_t prefixBytes;
```


### variable base

```cpp
uintptr_t base;
```


### variable addedByApplication

```cpp
bool addedByApplication;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100