---
title: _EsPOSIXSyscall

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| intptr_t | **[index](Classes/struct__EsPOSIXSyscall.md#variable-index)**  |
| intptr_t | **[arguments](Classes/struct__EsPOSIXSyscall.md#variable-arguments)**  |

## Public Attributes Documentation

### variable index

```cpp
intptr_t index;
```


### variable arguments

```cpp
intptr_t arguments;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100