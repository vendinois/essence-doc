---
title: EsProcessInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsProcessInformation.md#variable-handle)**  |
| uintptr_t | **[pid](Classes/structEsProcessInformation.md#variable-pid)**  |
| [EsThreadInformation](Classes/structEsThreadInformation.md) | **[mainThread](Classes/structEsProcessInformation.md#variable-mainthread)**  |

## Public Attributes Documentation

### variable handle

```cpp
EsHandle handle;
```


### variable pid

```cpp
uintptr_t pid;
```


### variable mainThread

```cpp
EsThreadInformation mainThread;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100