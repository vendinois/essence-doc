---
title: EsApplicationStartupRequest

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int64_t | **[id](Classes/structEsApplicationStartupRequest.md#variable-id)**  |
| const char * | **[filePath](Classes/structEsApplicationStartupRequest.md#variable-filepath)**  |
| ptrdiff_t | **[filePathBytes](Classes/structEsApplicationStartupRequest.md#variable-filepathbytes)**  |
| uint32_t | **[flags](Classes/structEsApplicationStartupRequest.md#variable-flags)**  |

## Public Attributes Documentation

### variable id

```cpp
int64_t id;
```


### variable filePath

```cpp
const char * filePath;
```


### variable filePathBytes

```cpp
ptrdiff_t filePathBytes;
```


### variable flags

```cpp
uint32_t flags;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100