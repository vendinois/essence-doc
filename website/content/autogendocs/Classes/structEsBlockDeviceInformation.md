---
title: EsBlockDeviceInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| size_t | **[sectorSize](Classes/structEsBlockDeviceInformation.md#variable-sectorsize)**  |
| [EsFileOffset](Files/api_8h.md#typedef-esfileoffset) | **[sectorCount](Classes/structEsBlockDeviceInformation.md#variable-sectorcount)**  |
| bool | **[readOnly](Classes/structEsBlockDeviceInformation.md#variable-readonly)**  |
| uint8_t | **[nestLevel](Classes/structEsBlockDeviceInformation.md#variable-nestlevel)**  |
| uint8_t | **[driveType](Classes/structEsBlockDeviceInformation.md#variable-drivetype)**  |
| uint8_t | **[modelBytes](Classes/structEsBlockDeviceInformation.md#variable-modelbytes)**  |
| char | **[model](Classes/structEsBlockDeviceInformation.md#variable-model)**  |

## Public Attributes Documentation

### variable sectorSize

```cpp
size_t sectorSize;
```


### variable sectorCount

```cpp
EsFileOffset sectorCount;
```


### variable readOnly

```cpp
bool readOnly;
```


### variable nestLevel

```cpp
uint8_t nestLevel;
```


### variable driveType

```cpp
uint8_t driveType;
```


### variable modelBytes

```cpp
uint8_t modelBytes;
```


### variable model

```cpp
char model;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100