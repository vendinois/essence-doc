---
title: EsMessageItemToString

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsGeneric](Classes/unionEsGeneric.md) | **[item](Classes/structEsMessageItemToString.md#variable-item)**  |
| const char * | **[text](Classes/structEsMessageItemToString.md#variable-text)**  |
| ptrdiff_t | **[textBytes](Classes/structEsMessageItemToString.md#variable-textbytes)**  |

## Public Attributes Documentation

### variable item

```cpp
EsGeneric item;
```


### variable text

```cpp
const char * text;
```


### variable textBytes

```cpp
ptrdiff_t textBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100