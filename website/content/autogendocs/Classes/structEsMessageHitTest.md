---
title: EsMessageHitTest

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[x](Classes/structEsMessageHitTest.md#variable-x)**  |
| int | **[y](Classes/structEsMessageHitTest.md#variable-y)**  |
| bool | **[inside](Classes/structEsMessageHitTest.md#variable-inside)**  |

## Public Attributes Documentation

### variable x

```cpp
int x;
```


### variable y

```cpp
int y;
```


### variable inside

```cpp
bool inside;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100