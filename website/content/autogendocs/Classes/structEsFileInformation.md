---
title: EsFileInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsFileInformation.md#variable-handle)**  |
| [EsFileOffset](Files/api_8h.md#typedef-esfileoffset) | **[size](Classes/structEsFileInformation.md#variable-size)**  |
| [EsError](Files/api_8h.md#typedef-eserror) | **[error](Classes/structEsFileInformation.md#variable-error)**  |

## Public Attributes Documentation

### variable handle

```cpp
EsHandle handle;
```


### variable size

```cpp
EsFileOffset size;
```


### variable error

```cpp
EsError error;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100