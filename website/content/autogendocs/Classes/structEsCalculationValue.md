---
title: EsCalculationValue

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| bool | **[error](Classes/structEsCalculationValue.md#variable-error)**  |
| double | **[number](Classes/structEsCalculationValue.md#variable-number)**  |

## Public Attributes Documentation

### variable error

```cpp
bool error;
```


### variable number

```cpp
double number;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100