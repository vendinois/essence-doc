---
title: EsINIState

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char * | **[buffer](Classes/structEsINIState.md#variable-buffer)**  |
| char * | **[sectionClass](Classes/structEsINIState.md#variable-sectionclass)**  |
| char * | **[section](Classes/structEsINIState.md#variable-section)**  |
| char * | **[key](Classes/structEsINIState.md#variable-key)**  |
| char * | **[value](Classes/structEsINIState.md#variable-value)**  |
| size_t | **[bytes](Classes/structEsINIState.md#variable-bytes)**  |
| size_t | **[sectionClassBytes](Classes/structEsINIState.md#variable-sectionclassbytes)**  |
| size_t | **[sectionBytes](Classes/structEsINIState.md#variable-sectionbytes)**  |
| size_t | **[keyBytes](Classes/structEsINIState.md#variable-keybytes)**  |
| size_t | **[valueBytes](Classes/structEsINIState.md#variable-valuebytes)**  |

## Public Attributes Documentation

### variable buffer

```cpp
char * buffer;
```


### variable sectionClass

```cpp
char * sectionClass;
```


### variable section

```cpp
char * section;
```


### variable key

```cpp
char * key;
```


### variable value

```cpp
char * value;
```


### variable bytes

```cpp
size_t bytes;
```


### variable sectionClassBytes

```cpp
size_t sectionClassBytes;
```


### variable sectionBytes

```cpp
size_t sectionBytes;
```


### variable keyBytes

```cpp
size_t keyBytes;
```


### variable valueBytes

```cpp
size_t valueBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100