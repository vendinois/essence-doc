---
title: EsProcessCreationArguments

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[executable](Classes/structEsProcessCreationArguments.md#variable-executable)**  |
| const [EsHandle](Files/api_8h.md#typedef-eshandle) * | **[handles](Classes/structEsProcessCreationArguments.md#variable-handles)**  |
| const uint32_t * | **[handleModes](Classes/structEsProcessCreationArguments.md#variable-handlemodes)**  |
| size_t | **[handleCount](Classes/structEsProcessCreationArguments.md#variable-handlecount)**  |
| [EsProcessCreateData](Classes/structEsProcessCreateData.md) | **[data](Classes/structEsProcessCreationArguments.md#variable-data)**  |
| uint32_t | **[flags](Classes/structEsProcessCreationArguments.md#variable-flags)**  |
| uint64_t | **[permissions](Classes/structEsProcessCreationArguments.md#variable-permissions)**  |

## Public Attributes Documentation

### variable executable

```cpp
EsHandle executable;
```


### variable handles

```cpp
const EsHandle * handles;
```


### variable handleModes

```cpp
const uint32_t * handleModes;
```


### variable handleCount

```cpp
size_t handleCount;
```


### variable data

```cpp
EsProcessCreateData data;
```


### variable flags

```cpp
uint32_t flags;
```


### variable permissions

```cpp
uint64_t permissions;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100