---
title: BundleFile

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint64_t | **[nameCRC64](Classes/structBundleFile.md#variable-namecrc64)**  |
| uint64_t | **[bytes](Classes/structBundleFile.md#variable-bytes)**  |
| uint64_t | **[offset](Classes/structBundleFile.md#variable-offset)**  |

## Public Attributes Documentation

### variable nameCRC64

```cpp
uint64_t nameCRC64;
```


### variable bytes

```cpp
uint64_t bytes;
```


### variable offset

```cpp
uint64_t offset;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100