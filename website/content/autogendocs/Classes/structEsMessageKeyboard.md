---
title: EsMessageKeyboard

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint16_t | **[scancode](Classes/structEsMessageKeyboard.md#variable-scancode)**  |
| uint8_t | **[modifiers](Classes/structEsMessageKeyboard.md#variable-modifiers)**  |
| bool | **[repeat](Classes/structEsMessageKeyboard.md#variable-repeat)**  |
| bool | **[numpad](Classes/structEsMessageKeyboard.md#variable-numpad)**  |
| bool | **[numlock](Classes/structEsMessageKeyboard.md#variable-numlock)**  |
| bool | **[single](Classes/structEsMessageKeyboard.md#variable-single)**  |

## Public Attributes Documentation

### variable scancode

```cpp
uint16_t scancode;
```


### variable modifiers

```cpp
uint8_t modifiers;
```


### variable repeat

```cpp
bool repeat;
```


### variable numpad

```cpp
bool numpad;
```


### variable numlock

```cpp
bool numlock;
```


### variable single

```cpp
bool single;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100