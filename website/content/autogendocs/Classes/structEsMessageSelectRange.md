---
title: EsMessageSelectRange

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[fromIndex](Classes/structEsMessageSelectRange.md#variable-fromindex)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[toIndex](Classes/structEsMessageSelectRange.md#variable-toindex)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageSelectRange.md#variable-group)**  |
| bool | **[select](Classes/structEsMessageSelectRange.md#variable-select)**  |
| bool | **[toggle](Classes/structEsMessageSelectRange.md#variable-toggle)**  |

## Public Attributes Documentation

### variable fromIndex

```cpp
EsListViewIndex fromIndex;
```


### variable toIndex

```cpp
EsListViewIndex toIndex;
```


### variable group

```cpp
EsListViewIndex group;
```


### variable select

```cpp
bool select;
```


### variable toggle

```cpp
bool toggle;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100