---
title: EsMessageItemRange

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageItemRange.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[firstIndex](Classes/structEsMessageItemRange.md#variable-firstindex)**  |
| uint64_t | **[count](Classes/structEsMessageItemRange.md#variable-count)**  |
| int64_t | **[result](Classes/structEsMessageItemRange.md#variable-result)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable firstIndex

```cpp
EsListViewIndex firstIndex;
```


### variable count

```cpp
uint64_t count;
```


### variable result

```cpp
int64_t result;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100