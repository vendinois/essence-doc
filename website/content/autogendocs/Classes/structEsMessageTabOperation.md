---
title: EsMessageTabOperation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[id](Classes/structEsMessageTabOperation.md#variable-id)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsMessageTabOperation.md#variable-handle)**  |
| size_t | **[bytes](Classes/structEsMessageTabOperation.md#variable-bytes)**  |
| bool | **[isSource](Classes/structEsMessageTabOperation.md#variable-issource)**  |
| union EsMessageTabOperation::@12 | **[@13](Classes/structEsMessageTabOperation.md#variable-@13)**  |
| [EsError](Files/api_8h.md#typedef-eserror) | **[error](Classes/structEsMessageTabOperation.md#variable-error)**  |

## Public Attributes Documentation

### variable id

```cpp
EsObjectID id;
```


### variable handle

```cpp
EsHandle handle;
```


### variable bytes

```cpp
size_t bytes;
```


### variable isSource

```cpp
bool isSource;
```


### variable @13

```cpp
union EsMessageTabOperation::@12 @13;
```


### variable error

```cpp
EsError error;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100