---
title: EsSnapshotProcesses

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| size_t | **[count](Classes/structEsSnapshotProcesses.md#variable-count)**  |
| [EsSnapshotProcessesItem](Classes/structEsSnapshotProcessesItem.md) | **[processes](Classes/structEsSnapshotProcesses.md#variable-processes)**  |

## Public Attributes Documentation

### variable count

```cpp
size_t count;
```


### variable processes

```cpp
EsSnapshotProcessesItem processes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100