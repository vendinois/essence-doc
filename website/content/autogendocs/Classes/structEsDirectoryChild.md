---
title: EsDirectoryChild

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char | **[name](Classes/structEsDirectoryChild.md#variable-name)**  |
| size_t | **[nameBytes](Classes/structEsDirectoryChild.md#variable-namebytes)**  |
| [EsNodeType](Files/api_8h.md#typedef-esnodetype) | **[type](Classes/structEsDirectoryChild.md#variable-type)**  |
| [EsFileOffsetDifference](Files/api_8h.md#typedef-esfileoffsetdifference) | **[fileSize](Classes/structEsDirectoryChild.md#variable-filesize)**  |
| [EsFileOffsetDifference](Files/api_8h.md#typedef-esfileoffsetdifference) | **[directoryChildren](Classes/structEsDirectoryChild.md#variable-directorychildren)**  |

## Public Attributes Documentation

### variable name

```cpp
char name;
```


### variable nameBytes

```cpp
size_t nameBytes;
```


### variable type

```cpp
EsNodeType type;
```


### variable fileSize

```cpp
EsFileOffsetDifference fileSize;
```


### variable directoryChildren

```cpp
EsFileOffsetDifference directoryChildren;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100