---
title: EsBuffer

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| const uint8_t * | **[in](Classes/structEsBuffer.md#variable-in)**  |
| uint8_t * | **[out](Classes/structEsBuffer.md#variable-out)**  |
| union EsBuffer::@0 | **[@1](Classes/structEsBuffer.md#variable-@1)**  |
| size_t | **[position](Classes/structEsBuffer.md#variable-position)**  |
| size_t | **[bytes](Classes/structEsBuffer.md#variable-bytes)**  |
| void * | **[context](Classes/structEsBuffer.md#variable-context)**  |
| [EsFileStore](Files/api_8h.md#define-esfilestore) * | **[fileStore](Classes/structEsBuffer.md#variable-filestore)**  |
| bool | **[error](Classes/structEsBuffer.md#variable-error)**  |
| bool | **[canGrow](Classes/structEsBuffer.md#variable-cangrow)**  |

## Public Attributes Documentation

### variable in

```cpp
const uint8_t * in;
```


### variable out

```cpp
uint8_t * out;
```


### variable @1

```cpp
union EsBuffer::@0 @1;
```


### variable position

```cpp
size_t position;
```


### variable bytes

```cpp
size_t bytes;
```


### variable context

```cpp
void * context;
```


### variable fileStore

```cpp
EsFileStore * fileStore;
```


### variable error

```cpp
bool error;
```


### variable canGrow

```cpp
bool canGrow;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100