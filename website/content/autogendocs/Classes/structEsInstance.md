---
title: EsInstance

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| void * | **[_private](Classes/structEsInstance.md#variable--private)**  |
| [EsWindow](Files/api_8h.md#define-eswindow) * | **[window](Classes/structEsInstance.md#variable-window)**  |
| [EsUndoManager](Files/api_8h.md#define-esundomanager) * | **[undoManager](Classes/structEsInstance.md#variable-undomanager)**  |
| [EsInstanceCallback](Files/api_8h.md#typedef-esinstancecallback) | **[callback](Classes/structEsInstance.md#variable-callback)**  |

## Public Attributes Documentation

### variable _private

```cpp
void * _private;
```


### variable window

```cpp
EsWindow * window;
```


### variable undoManager

```cpp
EsUndoManager * undoManager;
```


### variable callback

```cpp
EsInstanceCallback callback;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100