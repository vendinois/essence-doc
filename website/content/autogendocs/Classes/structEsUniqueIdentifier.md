---
title: EsUniqueIdentifier

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint8_t | **[d](Classes/structEsUniqueIdentifier.md#variable-d)**  |

## Public Attributes Documentation

### variable d

```cpp
uint8_t d;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100