---
title: EsMessage

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsMessageType](Files/api_8h.md#enum-esmessagetype) | **[type](Classes/structEsMessage.md#variable-type)**  |
| uintptr_t | **[_size](Classes/structEsMessage.md#variable--size)**  |
| struct EsMessage::@14::@16 | **[_size](Classes/structEsMessage.md#variable--size)**  |
| [EsMessageUser](Classes/structEsMessageUser.md) | **[user](Classes/structEsMessage.md#variable-user)**  |
| [EsMessageMouseMotion](Classes/structEsMessageMouseMotion.md) | **[mouseMoved](Classes/structEsMessage.md#variable-mousemoved)**  |
| [EsMessageMouseMotion](Classes/structEsMessageMouseMotion.md) | **[mouseDragged](Classes/structEsMessage.md#variable-mousedragged)**  |
| [EsMessageMouseButton](Classes/structEsMessageMouseButton.md) | **[mouseDown](Classes/structEsMessage.md#variable-mousedown)**  |
| [EsMessageKeyboard](Classes/structEsMessageKeyboard.md) | **[keyboard](Classes/structEsMessage.md#variable-keyboard)**  |
| [EsMessageWindowResized](Classes/structEsMessageWindowResized.md) | **[windowResized](Classes/structEsMessage.md#variable-windowresized)**  |
| [EsMessageAnimate](Classes/structEsMessageAnimate.md) | **[animate](Classes/structEsMessage.md#variable-animate)**  |
| [EsMessageLayout](Classes/structEsMessageLayout.md) | **[layout](Classes/structEsMessage.md#variable-layout)**  |
| [EsMessageMeasure](Classes/structEsMessageMeasure.md) | **[measure](Classes/structEsMessage.md#variable-measure)**  |
| [EsMessageHitTest](Classes/structEsMessageHitTest.md) | **[hitTest](Classes/structEsMessage.md#variable-hittest)**  |
| [EsMessageZOrder](Classes/structEsMessageZOrder.md) | **[zOrder](Classes/structEsMessage.md#variable-zorder)**  |
| [EsMessageBeforeZOrder](Classes/structEsMessageBeforeZOrder.md) | **[beforeZOrder](Classes/structEsMessage.md#variable-beforezorder)**  |
| [EsMessageItemToString](Classes/structEsMessageItemToString.md) | **[itemToString](Classes/structEsMessage.md#variable-itemtostring)**  |
| [EsMessageFocus](Classes/structEsMessageFocus.md) | **[focus](Classes/structEsMessage.md#variable-focus)**  |
| [EsMessageScrollWheel](Classes/structEsMessageScrollWheel.md) | **[scrollWheel](Classes/structEsMessage.md#variable-scrollwheel)**  |
| [EsMessageWindowActivated](Classes/structEsMessageWindowActivated.md) | **[windowActivated](Classes/structEsMessage.md#variable-windowactivated)**  |
| [EsMessageScroll](Classes/structEsMessageScroll.md) | **[scroll](Classes/structEsMessage.md#variable-scroll)**  |
| [EsMessageEnsureVisible](Classes/structEsMessageEnsureVisible.md) | **[ensureVisible](Classes/structEsMessage.md#variable-ensurevisible)**  |
| [EsStyleID](Files/api_8h.md#typedef-esstyleid) | **[childStyleVariant](Classes/structEsMessage.md#variable-childstylevariant)**  |
| [EsRectangle](Classes/structEsRectangle.md) * | **[accessKeyHintBounds](Classes/structEsMessage.md#variable-accesskeyhintbounds)**  |
| [EsPainter](Classes/structEsPainter.md) * | **[painter](Classes/structEsMessage.md#variable-painter)**  |
| [EsCursorStyle](Files/api_8h.md#enum-escursorstyle) | **[cursorStyle](Classes/structEsMessage.md#variable-cursorstyle)**  |
| [EsElement](Files/api_8h.md#define-eselement) * | **[child](Classes/structEsMessage.md#variable-child)**  |
| [EsMessageIterateIndex](Classes/structEsMessageIterateIndex.md) | **[iterateIndex](Classes/structEsMessage.md#variable-iterateindex)**  |
| [EsMessageItemRange](Classes/structEsMessageItemRange.md) | **[itemRange](Classes/structEsMessage.md#variable-itemrange)**  |
| [EsMessageMeasureItem](Classes/structEsMessageMeasureItem.md) | **[measureItem](Classes/structEsMessage.md#variable-measureitem)**  |
| [EsMessageCreateItem](Classes/structEsMessageCreateItem.md) | **[createItem](Classes/structEsMessage.md#variable-createitem)**  |
| [EsMessageGetContent](Classes/structEsMessageGetContent.md) | **[getContent](Classes/structEsMessage.md#variable-getcontent)**  |
| [EsMessageGetIndent](Classes/structEsMessageGetIndent.md) | **[getIndent](Classes/structEsMessage.md#variable-getindent)**  |
| [EsMessageSelectRange](Classes/structEsMessageSelectRange.md) | **[selectRange](Classes/structEsMessage.md#variable-selectrange)**  |
| [EsMessageSelectItem](Classes/structEsMessageSelectItem.md) | **[selectItem](Classes/structEsMessage.md#variable-selectitem)**  |
| [EsMessageChooseItem](Classes/structEsMessageChooseItem.md) | **[chooseItem](Classes/structEsMessage.md#variable-chooseitem)**  |
| [EsMessageSearchItem](Classes/structEsMessageSearchItem.md) | **[searchItem](Classes/structEsMessage.md#variable-searchitem)**  |
| [EsMessageColumnMenu](Classes/structEsMessageColumnMenu.md) | **[columnMenu](Classes/structEsMessage.md#variable-columnmenu)**  |
| [EsMessageGetColumnSort](Classes/structEsMessageGetColumnSort.md) | **[getColumnSort](Classes/structEsMessage.md#variable-getcolumnsort)**  |
| [EsMessageGetItemData](Classes/structEsMessageGetItemData.md) | **[getItemData](Classes/structEsMessage.md#variable-getitemdata)**  |
| [EsMessageSliderMoved](Classes/structEsMessageSliderMoved.md) | **[sliderMoved](Classes/structEsMessage.md#variable-slidermoved)**  |
| [EsMessageColorChanged](Classes/structEsMessageColorChanged.md) | **[colorChanged](Classes/structEsMessage.md#variable-colorchanged)**  |
| [EsMessageNumberDragDelta](Classes/structEsMessageNumberDragDelta.md) | **[numberDragDelta](Classes/structEsMessage.md#variable-numberdragdelta)**  |
| [EsMessageNumberUpdated](Classes/structEsMessageNumberUpdated.md) | **[numberUpdated](Classes/structEsMessage.md#variable-numberupdated)**  |
| [EsMessageGetBreadcrumb](Classes/structEsMessageGetBreadcrumb.md) | **[getBreadcrumb](Classes/structEsMessage.md#variable-getbreadcrumb)**  |
| [EsMessageEndEdit](Classes/structEsMessageEndEdit.md) | **[endEdit](Classes/structEsMessage.md#variable-endedit)**  |
| uintptr_t | **[activateBreadcrumb](Classes/structEsMessage.md#variable-activatebreadcrumb)**  |
| [EsCheckState](Files/api_8h.md#enum-escheckstate) | **[checkState](Classes/structEsMessage.md#variable-checkstate)**  |
| [EsMessageInstanceOpen](Classes/structEsMessageInstanceOpen.md) | **[instanceOpen](Classes/structEsMessage.md#variable-instanceopen)**  |
| [EsMessageInstanceSave](Classes/structEsMessageInstanceSave.md) | **[instanceSave](Classes/structEsMessage.md#variable-instancesave)**  |
| void * | **[_argument](Classes/structEsMessage.md#variable--argument)**  |
| [EsMessageProcessCrash](Classes/structEsMessageProcessCrash.md) | **[crash](Classes/structEsMessage.md#variable-crash)**  |
| [EsMessageEyedrop](Classes/structEsMessageEyedrop.md) | **[eyedrop](Classes/structEsMessage.md#variable-eyedrop)**  |
| [EsMessageCreateInstance](Classes/structEsMessageCreateInstance.md) | **[createInstance](Classes/structEsMessage.md#variable-createinstance)**  |
| [EsMessageTabOperation](Classes/structEsMessageTabOperation.md) | **[tabOperation](Classes/structEsMessage.md#variable-taboperation)**  |
| [EsMessageDevice](Classes/structEsMessageDevice.md) | **[device](Classes/structEsMessage.md#variable-device)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[embeddedWindowDestroyedID](Classes/structEsMessage.md#variable-embeddedwindowdestroyedid)**  |
| union EsMessage::@14 | **[@15](Classes/structEsMessage.md#variable-@15)**  |

## Public Attributes Documentation

### variable type

```cpp
EsMessageType type;
```


### variable _size

```cpp
uintptr_t _size;
```


### variable _size

```cpp
struct EsMessage::@14::@16 _size;
```


### variable user

```cpp
EsMessageUser user;
```


### variable mouseMoved

```cpp
EsMessageMouseMotion mouseMoved;
```


### variable mouseDragged

```cpp
EsMessageMouseMotion mouseDragged;
```


### variable mouseDown

```cpp
EsMessageMouseButton mouseDown;
```


### variable keyboard

```cpp
EsMessageKeyboard keyboard;
```


### variable windowResized

```cpp
EsMessageWindowResized windowResized;
```


### variable animate

```cpp
EsMessageAnimate animate;
```


### variable layout

```cpp
EsMessageLayout layout;
```


### variable measure

```cpp
EsMessageMeasure measure;
```


### variable hitTest

```cpp
EsMessageHitTest hitTest;
```


### variable zOrder

```cpp
EsMessageZOrder zOrder;
```


### variable beforeZOrder

```cpp
EsMessageBeforeZOrder beforeZOrder;
```


### variable itemToString

```cpp
EsMessageItemToString itemToString;
```


### variable focus

```cpp
EsMessageFocus focus;
```


### variable scrollWheel

```cpp
EsMessageScrollWheel scrollWheel;
```


### variable windowActivated

```cpp
EsMessageWindowActivated windowActivated;
```


### variable scroll

```cpp
EsMessageScroll scroll;
```


### variable ensureVisible

```cpp
EsMessageEnsureVisible ensureVisible;
```


### variable childStyleVariant

```cpp
EsStyleID childStyleVariant;
```


### variable accessKeyHintBounds

```cpp
EsRectangle * accessKeyHintBounds;
```


### variable painter

```cpp
EsPainter * painter;
```


### variable cursorStyle

```cpp
EsCursorStyle cursorStyle;
```


### variable child

```cpp
EsElement * child;
```


### variable iterateIndex

```cpp
EsMessageIterateIndex iterateIndex;
```


### variable itemRange

```cpp
EsMessageItemRange itemRange;
```


### variable measureItem

```cpp
EsMessageMeasureItem measureItem;
```


### variable createItem

```cpp
EsMessageCreateItem createItem;
```


### variable getContent

```cpp
EsMessageGetContent getContent;
```


### variable getIndent

```cpp
EsMessageGetIndent getIndent;
```


### variable selectRange

```cpp
EsMessageSelectRange selectRange;
```


### variable selectItem

```cpp
EsMessageSelectItem selectItem;
```


### variable chooseItem

```cpp
EsMessageChooseItem chooseItem;
```


### variable searchItem

```cpp
EsMessageSearchItem searchItem;
```


### variable columnMenu

```cpp
EsMessageColumnMenu columnMenu;
```


### variable getColumnSort

```cpp
EsMessageGetColumnSort getColumnSort;
```


### variable getItemData

```cpp
EsMessageGetItemData getItemData;
```


### variable sliderMoved

```cpp
EsMessageSliderMoved sliderMoved;
```


### variable colorChanged

```cpp
EsMessageColorChanged colorChanged;
```


### variable numberDragDelta

```cpp
EsMessageNumberDragDelta numberDragDelta;
```


### variable numberUpdated

```cpp
EsMessageNumberUpdated numberUpdated;
```


### variable getBreadcrumb

```cpp
EsMessageGetBreadcrumb getBreadcrumb;
```


### variable endEdit

```cpp
EsMessageEndEdit endEdit;
```


### variable activateBreadcrumb

```cpp
uintptr_t activateBreadcrumb;
```


### variable checkState

```cpp
EsCheckState checkState;
```


### variable instanceOpen

```cpp
EsMessageInstanceOpen instanceOpen;
```


### variable instanceSave

```cpp
EsMessageInstanceSave instanceSave;
```


### variable _argument

```cpp
void * _argument;
```


### variable crash

```cpp
EsMessageProcessCrash crash;
```


### variable eyedrop

```cpp
EsMessageEyedrop eyedrop;
```


### variable createInstance

```cpp
EsMessageCreateInstance createInstance;
```


### variable tabOperation

```cpp
EsMessageTabOperation tabOperation;
```


### variable device

```cpp
EsMessageDevice device;
```


### variable embeddedWindowDestroyedID

```cpp
EsObjectID embeddedWindowDestroyedID;
```


### variable @15

```cpp
union EsMessage::@14 @15;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100