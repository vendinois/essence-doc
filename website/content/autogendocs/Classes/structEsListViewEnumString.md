---
title: EsListViewEnumString

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| const char * | **[string](Classes/structEsListViewEnumString.md#variable-string)**  |
| ptrdiff_t | **[stringBytes](Classes/structEsListViewEnumString.md#variable-stringbytes)**  |

## Public Attributes Documentation

### variable string

```cpp
const char * string;
```


### variable stringBytes

```cpp
ptrdiff_t stringBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100