---
title: EsProcessState

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsCrashReason](Classes/structEsCrashReason.md) | **[crashReason](Classes/structEsProcessState.md#variable-crashreason)**  |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[id](Classes/structEsProcessState.md#variable-id)**  |
| uint8_t | **[executableState](Classes/structEsProcessState.md#variable-executablestate)**  |
| uint8_t | **[flags](Classes/structEsProcessState.md#variable-flags)**  |

## Public Attributes Documentation

### variable crashReason

```cpp
EsCrashReason crashReason;
```


### variable id

```cpp
EsObjectID id;
```


### variable executableState

```cpp
uint8_t executableState;
```


### variable flags

```cpp
uint8_t flags;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100