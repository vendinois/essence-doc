---
title: EsConnection

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsAddress](Classes/structEsAddress.md) | **[address](Classes/structEsConnection.md#variable-address)**  |
| size_t | **[receiveBufferBytes](Classes/structEsConnection.md#variable-receivebufferbytes)**  |
| size_t | **[sendBufferBytes](Classes/structEsConnection.md#variable-sendbufferbytes)**  |
| uint8_t * | **[receiveBuffer](Classes/structEsConnection.md#variable-receivebuffer)**  |
| uint8_t * | **[sendBuffer](Classes/structEsConnection.md#variable-sendbuffer)**  |
| uintptr_t | **[receiveWritePointer](Classes/structEsConnection.md#variable-receivewritepointer)**  |
| uintptr_t | **[sendReadPointer](Classes/structEsConnection.md#variable-sendreadpointer)**  |
| bool | **[open](Classes/structEsConnection.md#variable-open)**  |
| [EsError](Files/api_8h.md#typedef-eserror) | **[error](Classes/structEsConnection.md#variable-error)**  |
| uintptr_t | **[receiveReadPointer](Classes/structEsConnection.md#variable-receivereadpointer)**  |
| uintptr_t | **[sendWritePointer](Classes/structEsConnection.md#variable-sendwritepointer)**  |
| [EsHandle](Files/api_8h.md#typedef-eshandle) | **[handle](Classes/structEsConnection.md#variable-handle)**  |

## Public Attributes Documentation

### variable address

```cpp
EsAddress address;
```


### variable receiveBufferBytes

```cpp
size_t receiveBufferBytes;
```


### variable sendBufferBytes

```cpp
size_t sendBufferBytes;
```


### variable receiveBuffer

```cpp
uint8_t * receiveBuffer;
```


### variable sendBuffer

```cpp
uint8_t * sendBuffer;
```


### variable receiveWritePointer

```cpp
uintptr_t receiveWritePointer;
```


### variable sendReadPointer

```cpp
uintptr_t sendReadPointer;
```


### variable open

```cpp
bool open;
```


### variable error

```cpp
EsError error;
```


### variable receiveReadPointer

```cpp
uintptr_t receiveReadPointer;
```


### variable sendWritePointer

```cpp
uintptr_t sendWritePointer;
```


### variable handle

```cpp
EsHandle handle;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100