---
title: EsSnapshotProcessesItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int64_t | **[pid](Classes/structEsSnapshotProcessesItem.md#variable-pid)**  |
| int64_t | **[memoryUsage](Classes/structEsSnapshotProcessesItem.md#variable-memoryusage)**  |
| int64_t | **[cpuTimeSlices](Classes/structEsSnapshotProcessesItem.md#variable-cputimeslices)**  |
| int64_t | **[idleTimeSlices](Classes/structEsSnapshotProcessesItem.md#variable-idletimeslices)**  |
| int64_t | **[handleCount](Classes/structEsSnapshotProcessesItem.md#variable-handlecount)**  |
| int64_t | **[threadCount](Classes/structEsSnapshotProcessesItem.md#variable-threadcount)**  |
| char | **[name](Classes/structEsSnapshotProcessesItem.md#variable-name)**  |
| uint8_t | **[nameBytes](Classes/structEsSnapshotProcessesItem.md#variable-namebytes)**  |
| bool | **[isKernel](Classes/structEsSnapshotProcessesItem.md#variable-iskernel)**  |

## Public Attributes Documentation

### variable pid

```cpp
int64_t pid;
```


### variable memoryUsage

```cpp
int64_t memoryUsage;
```


### variable cpuTimeSlices

```cpp
int64_t cpuTimeSlices;
```


### variable idleTimeSlices

```cpp
int64_t idleTimeSlices;
```


### variable handleCount

```cpp
int64_t handleCount;
```


### variable threadCount

```cpp
int64_t threadCount;
```


### variable name

```cpp
char name;
```


### variable nameBytes

```cpp
uint8_t nameBytes;
```


### variable isKernel

```cpp
bool isKernel;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100