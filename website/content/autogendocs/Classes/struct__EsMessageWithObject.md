---
title: _EsMessageWithObject

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| void * | **[object](Classes/struct__EsMessageWithObject.md#variable-object)**  |
| [EsMessage](Classes/structEsMessage.md) | **[message](Classes/struct__EsMessageWithObject.md#variable-message)**  |

## Public Attributes Documentation

### variable object

```cpp
void * object;
```


### variable message

```cpp
EsMessage message;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100