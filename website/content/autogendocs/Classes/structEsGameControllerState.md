---
title: EsGameControllerState

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsObjectID](Files/api_8h.md#typedef-esobjectid) | **[id](Classes/structEsGameControllerState.md#variable-id)**  |
| uint8_t | **[buttonCount](Classes/structEsGameControllerState.md#variable-buttoncount)**  |
| uint8_t | **[analogCount](Classes/structEsGameControllerState.md#variable-analogcount)**  |
| uint8_t | **[directionalPad](Classes/structEsGameControllerState.md#variable-directionalpad)**  |
| uint32_t | **[buttons](Classes/structEsGameControllerState.md#variable-buttons)**  |
| [EsAnalogInput](Classes/structEsAnalogInput.md) | **[analog](Classes/structEsGameControllerState.md#variable-analog)**  |

## Public Attributes Documentation

### variable id

```cpp
EsObjectID id;
```


### variable buttonCount

```cpp
uint8_t buttonCount;
```


### variable analogCount

```cpp
uint8_t analogCount;
```


### variable directionalPad

```cpp
uint8_t directionalPad;
```


### variable buttons

```cpp
uint32_t buttons;
```


### variable analog

```cpp
EsAnalogInput analog;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100