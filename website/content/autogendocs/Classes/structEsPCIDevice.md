---
title: EsPCIDevice

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| uint32_t | **[deviceID](Classes/structEsPCIDevice.md#variable-deviceid)**  |
| uint8_t | **[classCode](Classes/structEsPCIDevice.md#variable-classcode)**  |
| uint8_t | **[subclassCode](Classes/structEsPCIDevice.md#variable-subclasscode)**  |
| uint8_t | **[progIF](Classes/structEsPCIDevice.md#variable-progif)**  |
| uint8_t | **[bus](Classes/structEsPCIDevice.md#variable-bus)**  |
| uint8_t | **[slot](Classes/structEsPCIDevice.md#variable-slot)**  |
| uint8_t | **[function](Classes/structEsPCIDevice.md#variable-function)**  |
| uint8_t | **[interruptPin](Classes/structEsPCIDevice.md#variable-interruptpin)**  |
| uint8_t | **[interruptLine](Classes/structEsPCIDevice.md#variable-interruptline)**  |
| size_t | **[baseAddressesSizes](Classes/structEsPCIDevice.md#variable-baseaddressessizes)**  |
| uint32_t | **[baseAddresses](Classes/structEsPCIDevice.md#variable-baseaddresses)**  |
| char | **[driverName](Classes/structEsPCIDevice.md#variable-drivername)**  |
| size_t | **[driverNameBytes](Classes/structEsPCIDevice.md#variable-drivernamebytes)**  |

## Public Attributes Documentation

### variable deviceID

```cpp
uint32_t deviceID;
```


### variable classCode

```cpp
uint8_t classCode;
```


### variable subclassCode

```cpp
uint8_t subclassCode;
```


### variable progIF

```cpp
uint8_t progIF;
```


### variable bus

```cpp
uint8_t bus;
```


### variable slot

```cpp
uint8_t slot;
```


### variable function

```cpp
uint8_t function;
```


### variable interruptPin

```cpp
uint8_t interruptPin;
```


### variable interruptLine

```cpp
uint8_t interruptLine;
```


### variable baseAddressesSizes

```cpp
size_t baseAddressesSizes;
```


### variable baseAddresses

```cpp
uint32_t baseAddresses;
```


### variable driverName

```cpp
char driverName;
```


### variable driverNameBytes

```cpp
size_t driverNameBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100