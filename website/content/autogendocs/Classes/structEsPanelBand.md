---
title: EsPanelBand

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| int | **[preferredSize](Classes/structEsPanelBand.md#variable-preferredsize)**  |
| int | **[minimumSize](Classes/structEsPanelBand.md#variable-minimumsize)**  |
| int | **[maximumSize](Classes/structEsPanelBand.md#variable-maximumsize)**  |
| int | **[push](Classes/structEsPanelBand.md#variable-push)**  |
| int | **[pull](Classes/structEsPanelBand.md#variable-pull)**  |

## Public Attributes Documentation

### variable preferredSize

```cpp
int preferredSize;
```


### variable minimumSize

```cpp
int minimumSize;
```


### variable maximumSize

```cpp
int maximumSize;
```


### variable push

```cpp
int push;
```


### variable pull

```cpp
int pull;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100