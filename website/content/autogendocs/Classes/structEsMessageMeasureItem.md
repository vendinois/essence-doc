---
title: EsMessageMeasureItem

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageMeasureItem.md#variable-group)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageMeasureItem.md#variable-index)**  |
| int64_t | **[result](Classes/structEsMessageMeasureItem.md#variable-result)**  |

## Public Attributes Documentation

### variable group

```cpp
EsListViewIndex group;
```


### variable index

```cpp
EsListViewIndex index;
```


### variable result

```cpp
int64_t result;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100