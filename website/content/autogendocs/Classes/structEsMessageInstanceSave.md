---
title: EsMessageInstanceSave

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsFileStore](Files/api_8h.md#define-esfilestore) * | **[file](Classes/structEsMessageInstanceSave.md#variable-file)**  |
| const char * | **[nameOrPath](Classes/structEsMessageInstanceSave.md#variable-nameorpath)**  |
| ptrdiff_t | **[nameOrPathBytes](Classes/structEsMessageInstanceSave.md#variable-nameorpathbytes)**  |

## Public Attributes Documentation

### variable file

```cpp
EsFileStore * file;
```


### variable nameOrPath

```cpp
const char * nameOrPath;
```


### variable nameOrPathBytes

```cpp
ptrdiff_t nameOrPathBytes;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100