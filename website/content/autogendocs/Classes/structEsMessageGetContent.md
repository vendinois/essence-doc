---
title: EsMessageGetContent

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageGetContent.md#variable-index)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageGetContent.md#variable-group)**  |
| [EsBuffer](Classes/structEsBuffer.md) * | **[buffer](Classes/structEsMessageGetContent.md#variable-buffer)**  |
| uint32_t | **[icon](Classes/structEsMessageGetContent.md#variable-icon)**  |
| uint32_t | **[drawContentFlags](Classes/structEsMessageGetContent.md#variable-drawcontentflags)**  |
| uint32_t | **[columnID](Classes/structEsMessageGetContent.md#variable-columnid)**  |
| uint16_t | **[activeColumnIndex](Classes/structEsMessageGetContent.md#variable-activecolumnindex)**  |

## Public Attributes Documentation

### variable index

```cpp
EsListViewIndex index;
```


### variable group

```cpp
EsListViewIndex group;
```


### variable buffer

```cpp
EsBuffer * buffer;
```


### variable icon

```cpp
uint32_t icon;
```


### variable drawContentFlags

```cpp
uint32_t drawContentFlags;
```


### variable columnID

```cpp
uint32_t columnID;
```


### variable activeColumnIndex

```cpp
uint16_t activeColumnIndex;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100