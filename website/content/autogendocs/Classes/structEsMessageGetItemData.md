---
title: EsMessageGetItemData

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[index](Classes/structEsMessageGetItemData.md#variable-index)**  |
| [EsListViewIndex](Files/api_8h.md#typedef-eslistviewindex) | **[group](Classes/structEsMessageGetItemData.md#variable-group)**  |
| uint32_t | **[columnID](Classes/structEsMessageGetItemData.md#variable-columnid)**  |
| uint16_t | **[activeColumnIndex](Classes/structEsMessageGetItemData.md#variable-activecolumnindex)**  |
| uint32_t | **[icon](Classes/structEsMessageGetItemData.md#variable-icon)**  |
| const char * | **[s](Classes/structEsMessageGetItemData.md#variable-s)**  |
| ptrdiff_t | **[sBytes](Classes/structEsMessageGetItemData.md#variable-sbytes)**  |
| int64_t | **[i](Classes/structEsMessageGetItemData.md#variable-i)**  |
| double | **[d](Classes/structEsMessageGetItemData.md#variable-d)**  |
| union EsMessageGetItemData::@8 | **[@9](Classes/structEsMessageGetItemData.md#variable-@9)**  |

## Public Attributes Documentation

### variable index

```cpp
EsListViewIndex index;
```


### variable group

```cpp
EsListViewIndex group;
```


### variable columnID

```cpp
uint32_t columnID;
```


### variable activeColumnIndex

```cpp
uint16_t activeColumnIndex;
```


### variable icon

```cpp
uint32_t icon;
```


### variable s

```cpp
const char * s;
```


### variable sBytes

```cpp
ptrdiff_t sBytes;
```


### variable i

```cpp
int64_t i;
```


### variable d

```cpp
double d;
```


### variable @9

```cpp
union EsMessageGetItemData::@8 @9;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100