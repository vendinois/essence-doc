---
title: EsFontInformation

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| char | **[name](Classes/structEsFontInformation.md#variable-name)**  |
| size_t | **[nameBytes](Classes/structEsFontInformation.md#variable-namebytes)**  |
| char | **[category](Classes/structEsFontInformation.md#variable-category)**  |
| size_t | **[categoryBytes](Classes/structEsFontInformation.md#variable-categorybytes)**  |
| [EsFontFamily](Files/api_8h.md#typedef-esfontfamily) | **[id](Classes/structEsFontInformation.md#variable-id)**  |
| uint16_t | **[availableWeightsNormal](Classes/structEsFontInformation.md#variable-availableweightsnormal)**  |
| uint16_t | **[availableWeightsItalic](Classes/structEsFontInformation.md#variable-availableweightsitalic)**  |

## Public Attributes Documentation

### variable name

```cpp
char name;
```


### variable nameBytes

```cpp
size_t nameBytes;
```


### variable category

```cpp
char category;
```


### variable categoryBytes

```cpp
size_t categoryBytes;
```


### variable id

```cpp
EsFontFamily id;
```


### variable availableWeightsNormal

```cpp
uint16_t availableWeightsNormal;
```


### variable availableWeightsItalic

```cpp
uint16_t availableWeightsItalic;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100