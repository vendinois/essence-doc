---
title: EsTextRun

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsTextStyle](Classes/structEsTextStyle.md) | **[style](Classes/structEsTextRun.md#variable-style)**  |
| uint32_t | **[offset](Classes/structEsTextRun.md#variable-offset)**  |

## Public Attributes Documentation

### variable style

```cpp
EsTextStyle style;
```


### variable offset

```cpp
uint32_t offset;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100