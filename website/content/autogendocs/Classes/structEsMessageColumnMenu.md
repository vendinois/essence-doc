---
title: EsMessageColumnMenu

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsElement](Files/api_8h.md#define-eselement) * | **[source](Classes/structEsMessageColumnMenu.md#variable-source)**  |
| uint32_t | **[columnID](Classes/structEsMessageColumnMenu.md#variable-columnid)**  |
| uint16_t | **[activeColumnIndex](Classes/structEsMessageColumnMenu.md#variable-activecolumnindex)**  |

## Public Attributes Documentation

### variable source

```cpp
EsElement * source;
```


### variable columnID

```cpp
uint32_t columnID;
```


### variable activeColumnIndex

```cpp
uint16_t activeColumnIndex;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100