---
title: EsTextSelection

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| ptrdiff_t | **[caret0](Classes/structEsTextSelection.md#variable-caret0)**  |
| ptrdiff_t | **[caret1](Classes/structEsTextSelection.md#variable-caret1)**  |
| bool | **[hideCaret](Classes/structEsTextSelection.md#variable-hidecaret)**  |
| bool | **[snapCaretToInsets](Classes/structEsTextSelection.md#variable-snapcarettoinsets)**  |
| uint32_t | **[foreground](Classes/structEsTextSelection.md#variable-foreground)**  |
| uint32_t | **[background](Classes/structEsTextSelection.md#variable-background)**  |

## Public Attributes Documentation

### variable caret0

```cpp
ptrdiff_t caret0;
```


### variable caret1

```cpp
ptrdiff_t caret1;
```


### variable hideCaret

```cpp
bool hideCaret;
```


### variable snapCaretToInsets

```cpp
bool snapCaretToInsets;
```


### variable foreground

```cpp
uint32_t foreground;
```


### variable background

```cpp
uint32_t background;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100