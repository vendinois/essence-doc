---
title: EsMessageInstanceOpen

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsFileStore](Files/api_8h.md#define-esfilestore) * | **[file](Classes/structEsMessageInstanceOpen.md#variable-file)**  |
| const char * | **[nameOrPath](Classes/structEsMessageInstanceOpen.md#variable-nameorpath)**  |
| ptrdiff_t | **[nameOrPathBytes](Classes/structEsMessageInstanceOpen.md#variable-nameorpathbytes)**  |
| bool | **[update](Classes/structEsMessageInstanceOpen.md#variable-update)**  |

## Public Attributes Documentation

### variable file

```cpp
EsFileStore * file;
```


### variable nameOrPath

```cpp
const char * nameOrPath;
```


### variable nameOrPathBytes

```cpp
ptrdiff_t nameOrPathBytes;
```


### variable update

```cpp
bool update;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100