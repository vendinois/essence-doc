---
title: EsBatchCall

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsSyscallType](Files/api_8h.md#enum-essyscalltype) | **[index](Classes/structEsBatchCall.md#variable-index)**  |
| bool | **[stopBatchIfError](Classes/structEsBatchCall.md#variable-stopbatchiferror)**  |
| uintptr_t | **[argument0](Classes/structEsBatchCall.md#variable-argument0)**  |
| uintptr_t | **[returnValue](Classes/structEsBatchCall.md#variable-returnvalue)**  |
| union EsBatchCall::@2 | **[@3](Classes/structEsBatchCall.md#variable-@3)**  |
| uintptr_t | **[argument1](Classes/structEsBatchCall.md#variable-argument1)**  |
| uintptr_t | **[argument2](Classes/structEsBatchCall.md#variable-argument2)**  |
| uintptr_t | **[argument3](Classes/structEsBatchCall.md#variable-argument3)**  |

## Public Attributes Documentation

### variable index

```cpp
EsSyscallType index;
```


### variable stopBatchIfError

```cpp
bool stopBatchIfError;
```


### variable argument0

```cpp
uintptr_t argument0;
```


### variable returnValue

```cpp
uintptr_t returnValue;
```


### variable @3

```cpp
union EsBatchCall::@2 @3;
```


### variable argument1

```cpp
uintptr_t argument1;
```


### variable argument2

```cpp
uintptr_t argument2;
```


### variable argument3

```cpp
uintptr_t argument3;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100