---
title: EsMemoryStatistics

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| size_t | **[fixedHeapAllocationCount](Classes/structEsMemoryStatistics.md#variable-fixedheapallocationcount)**  |
| size_t | **[fixedHeapTotalSize](Classes/structEsMemoryStatistics.md#variable-fixedheaptotalsize)**  |
| size_t | **[coreHeapAllocationCount](Classes/structEsMemoryStatistics.md#variable-coreheapallocationcount)**  |
| size_t | **[coreHeapTotalSize](Classes/structEsMemoryStatistics.md#variable-coreheaptotalsize)**  |
| size_t | **[cachedNodes](Classes/structEsMemoryStatistics.md#variable-cachednodes)**  |
| size_t | **[cachedDirectoryEntries](Classes/structEsMemoryStatistics.md#variable-cacheddirectoryentries)**  |
| size_t | **[totalSurfaceBytes](Classes/structEsMemoryStatistics.md#variable-totalsurfacebytes)**  |
| size_t | **[commitPageable](Classes/structEsMemoryStatistics.md#variable-commitpageable)**  |
| size_t | **[commitFixed](Classes/structEsMemoryStatistics.md#variable-commitfixed)**  |
| size_t | **[commitLimit](Classes/structEsMemoryStatistics.md#variable-commitlimit)**  |
| size_t | **[commitFixedLimit](Classes/structEsMemoryStatistics.md#variable-commitfixedlimit)**  |
| size_t | **[commitRemaining](Classes/structEsMemoryStatistics.md#variable-commitremaining)**  |
| size_t | **[maximumObjectCachePages](Classes/structEsMemoryStatistics.md#variable-maximumobjectcachepages)**  |
| size_t | **[approximateObjectCacheSize](Classes/structEsMemoryStatistics.md#variable-approximateobjectcachesize)**  |
| size_t | **[countZeroedPages](Classes/structEsMemoryStatistics.md#variable-countzeroedpages)**  |
| size_t | **[countFreePages](Classes/structEsMemoryStatistics.md#variable-countfreepages)**  |
| size_t | **[countStandbyPages](Classes/structEsMemoryStatistics.md#variable-countstandbypages)**  |
| size_t | **[countActivePages](Classes/structEsMemoryStatistics.md#variable-countactivepages)**  |

## Public Attributes Documentation

### variable fixedHeapAllocationCount

```cpp
size_t fixedHeapAllocationCount;
```


### variable fixedHeapTotalSize

```cpp
size_t fixedHeapTotalSize;
```


### variable coreHeapAllocationCount

```cpp
size_t coreHeapAllocationCount;
```


### variable coreHeapTotalSize

```cpp
size_t coreHeapTotalSize;
```


### variable cachedNodes

```cpp
size_t cachedNodes;
```


### variable cachedDirectoryEntries

```cpp
size_t cachedDirectoryEntries;
```


### variable totalSurfaceBytes

```cpp
size_t totalSurfaceBytes;
```


### variable commitPageable

```cpp
size_t commitPageable;
```


### variable commitFixed

```cpp
size_t commitFixed;
```


### variable commitLimit

```cpp
size_t commitLimit;
```


### variable commitFixedLimit

```cpp
size_t commitFixedLimit;
```


### variable commitRemaining

```cpp
size_t commitRemaining;
```


### variable maximumObjectCachePages

```cpp
size_t maximumObjectCachePages;
```


### variable approximateObjectCacheSize

```cpp
size_t approximateObjectCacheSize;
```


### variable countZeroedPages

```cpp
size_t countZeroedPages;
```


### variable countFreePages

```cpp
size_t countFreePages;
```


### variable countStandbyPages

```cpp
size_t countStandbyPages;
```


### variable countActivePages

```cpp
size_t countActivePages;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100