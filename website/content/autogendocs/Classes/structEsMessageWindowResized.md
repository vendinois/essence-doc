---
title: EsMessageWindowResized

---



## Public Attributes

|                | Name           |
| -------------- | -------------- |
| [EsRectangle](Classes/structEsRectangle.md) | **[content](Classes/structEsMessageWindowResized.md#variable-content)**  |
| bool | **[hidden](Classes/structEsMessageWindowResized.md#variable-hidden)**  |

## Public Attributes Documentation

### variable content

```cpp
EsRectangle content;
```


### variable hidden

```cpp
bool hidden;
```


-------------------------------

Updated on 2022-02-13 at 18:29:59 +0100