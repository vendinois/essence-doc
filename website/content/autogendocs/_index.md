---
title: Api Reference
---
Here you can find the Api reference for Essence.

It is generated automatically from the C API header. More information about this can be found here: [Generating the API header](../docs/building/#generating-the-api-header).

The documentation generation process is as follow:
* Generate the api header.
* Run Doxygen to generate the XML documentation.
* Run Doxybook2 to generate Markdown files from the Doxygen's XML files. 
* Import the generated doc into this website.

The repository with all the sources for the documentation generation can be found here: [Essence-doc](https://gitlab.com/vendinois/essence-doc).