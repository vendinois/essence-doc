---
title: Building
slug: building
weight: 2
---
# Building Essence

{{< tip "warning" >}}
**Warning: This software is still in development. Expect bugs.**
{{< /tip >}}

## Linux

These instructions expect you to be running an updated modern Linux distribution. See below for additional information about specific distributions.

Download this project's source. 

```sh
$> git clone --depth=1 https://gitlab.com/nakst/essence.git/
```

Start the build system.

```sh
$> ./start.sh
```

The compiler toolchain will be downloaded.

Once complete, you can test the operating system in an emulator. 
* If you have Qemu and KVM installed, run `k` in the build system. **Recommended!**
* If you have Qemu installed, run `t2` in the build system.
* If you have VirtualBox installed, make a 128MB drive called `vbox.vdi` in the `bin` folder, attach it to a virtual machine called "Essence" (choose "Windows 7 64-bit" as the OS), and run `v` in the build system.

Run `build-port` in the build system to view a list of optional ports that can be built.

### Arch Linux

You can install all the needed dependencies with:
```sh
$> sudo pacman -S base-devel curl nasm gmp mpfr mpc ctags qemu
```

Run `./start k` to build and run the system in Qemu.

### Fedora Linux

You can install all the needed dependencies with:
```sh
dnf install nasm gmp gmp-devel mpfr mpfr-devel libmpc libmpc-devel
```

Run `./start k` to build the system. Then run `qemu-kvm bin/drive` to launch the built image in Qemu.

### Ubuntu

Essence is built using GitHub Actions using their Ubuntu virtual machines. [This file contains a list of additional dependencies that need to be installed](https://github.com/nakst/build-essence/blob/main/.github/workflows/build-essence.yml), and [this is the build script that actually performs the build](https://github.com/nakst/build-essence/blob/main/build.sh).

## macOS

Install `brew` and then run:
```sh
$> brew install gcc@11 nasm ctags xz coreutils gnu-sed
``` 

Follow the instructions above for Linux to build and test the system. KVM will not be available.

## Configuration

From within the build system, run the command `config` to open the configuration editor. Click an option to change its value, and then click the `Save` button. You changes are saved locally, and will not be uploaded by Git. Not all configurations are likely to work; if you don't know what you're doing, it's probably best to stick with the defaults.

### Keyboard layout

To set the default keyboard layout for use in the emulator to match your current one, run:

```sh
$> setxkbmap -query | grep layout | awk '{OFS=""; print "General.keyboard_layout=", $2}' >> bin/config.ini
```

## Generating the API header

If you want your project to target Essence, you need to generate the API header for your programming language of choice.

```sh
$> gcc -o bin/build_core util/build_core.c 
$> bin/build_core headers <language> <path-to-output-file>
```

Currently supported languages are: 
* C (also works for C++)
* Zig 
* Odin

{{< tip >}}
Documentation for the API is still a work in progress.
{{< /tip >}}

For examples of how to use the API, consult the standard applications in the [apps](https://gitlab.com/nakst/essence/-/tree/master/apps) folder of the source tree.

Minimal sample applications are placed in the [apps/samples](https://gitlab.com/nakst/essence/-/tree/master/apps/samples) folder.

By placing your application's `.ini` file in the `apps/` folder, it will be automatically built by the build system.

## Where to next?

For a more thorough documentation of the build system, see [Build system](02_build_system.md).

For an overview of the files in the source tree, see [Source map](03_source_map.md).

For guidance on contributing to the project, see [Contributing](05_contributing).

For a list of things that need to be worked on, see [Todo](06_todo.md).

For discussion, join our Discord server: [Essence - Discord](https://discord.gg/skeP9ZGDK8).
