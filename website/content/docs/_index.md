---
title: Documentation
---
Here you can find information on how to test Essence. Either by [testing](testing.md) one of the nightly releases inside a VM or [building](building.md) it yourself.

## Links and Community

For discussion, join our Discord server: https://discord.gg/skeP9ZGDK8. 

Alternatively, visit the forums (not very active): https://essence.handmade.network/forums.

To support development, you can donate to Nakst Patreon: https://www.patreon.com/nakst.