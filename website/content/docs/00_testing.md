---
title: Testing
slug: testing
weight: 1
---
You can download and test the latest nightly build from https://github.com/nakst/build-essence/releases. 

* Download and extract `Essence.tar.xz`. 
  * If you are using VirtualBox, import `Essence.ova`. 
  * If you are using Qemu, run: 

```
qemu-kvm -drive file=drive,format=raw -m 2048 -smp 2 || qemu-system-x86_64 -enable-kvm -drive file=drive,format=raw -m 2048 -smp 2
```

These builds are configured to run on emulators only, to make testing easier. Builds for real hardware are coming soon :)
